FROM python:3.10-slim

# Install tzdata for timezone configuration
RUN apt-get update && apt-get install -y tzdata

# Set the timezone to Berlin
ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Configure Poetry
ENV POETRY_VERSION=1.8.2
ENV POETRY_HOME=/opt/poetry
ENV POETRY_VENV=/opt/poetry-venv
ENV POETRY_CACHE_DIR=/opt/.cache

# Install poetry separated from system interpreter
RUN python3 -m venv $POETRY_VENV \
    && $POETRY_VENV/bin/pip install -U pip setuptools \
    && $POETRY_VENV/bin/pip install poetry==${POETRY_VERSION}

# Add `poetry` to PATH
WORKDIR /usr/src

ENV DATA_DIR=/opt/data
ENV PATH="${PATH}:${POETRY_VENV}/bin:/usr/src:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

COPY data_harvesting ./data_harvesting
COPY pyproject.toml ./
COPY README.md ./
COPY LICENSES ./
COPY external_data ./external_data
COPY tests/manual_pipeline_test ./minimal_test_run

RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction --no-cache --without dev

# create a test-pool as process pool
RUN prefect config set PREFECT_API_URL=http://prefect-server:4200/api
RUN prefect config set PREFECT_LOGGING_LEVEL=DEBUG
RUN export PREFECT_LOGGING_EXTRA_LOGGERS=data_harvesting,hmc-unhide

COPY start-worker.sh /start-worker.sh
CMD ["sh", "-c", "/start-worker.sh"]
