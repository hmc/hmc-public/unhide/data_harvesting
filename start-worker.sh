#!/bin/sh

# Check if the work pool exists
if prefect work-pool ls | grep -q 'test-pool'; then
    echo "Work pool 'test-pool' already exists."
else
    echo "Creating work pool 'test-pool'."
    prefect work-pool create test-pool --type process
fi

# Start the worker
prefect worker start --pool 'test-pool'
