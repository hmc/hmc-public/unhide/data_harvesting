# -*- coding: utf-8 -*-
"""Module containing tests for patches creation of json-ld/rdf graphs"""
import rdflib
from rdflib import URIRef
from rdflib.compare import graph_diff, isomorphic
from rdflib.namespace import FOAF

from data_harvesting.util.rdf_util import Graph, copy_graph
from data_harvesting.rdfpatch import RDFPatch
from data_harvesting.rdfpatch import generate_patch


def mk_dummy(val: str):
    return URIRef(f'http://example.com/{val}')


def init_graph():
    """
    provides a simple test graph
    """
    graph = Graph()
    graph.add((mk_dummy('1'), FOAF.name, mk_dummy('2')))
    graph.add((mk_dummy('2'), FOAF.name, mk_dummy('3')))
    graph.add((mk_dummy('3'), FOAF.name, mk_dummy('4')))
    graph.add((mk_dummy('4'), FOAF.name, mk_dummy('5')))

    return graph


def test_generate_patch_from_graph():
    """
    Test patch generation works
    """
    graph = init_graph()
    out_graph = copy_graph(init_graph())

    out_graph.remove((mk_dummy('4'), FOAF.name, mk_dummy('5')))

    in_both, in_first, in_second = graph_diff(graph, out_graph)
    patch = RDFPatch.from_graph_diff(in_first, in_second)

    print(type(patch))
    print(patch.model_dump())
    print(patch.model_dump_json())
    patch_string = patch.model_dump()
    print(patch_string)

    assert isinstance(patch, RDFPatch)
    # assert 'TX .\nD 4 http://xmlns.com/foaf/0.1/name 5 .\nTC .' in patch_string

    assert len(patch.delete_triples) > 0
    assert len(patch.add_triples) == 0
    triple = (mk_dummy('4'), FOAF.name, mk_dummy('5'))
    assert triple in patch.delete_triples  # pylint: disable unsupported-membership-test


@generate_patch()  # graph_key='graph', patch_format='jena')
def simple_function(graph):
    """
    delete the first triple (3, name, 4) and add one triple (5 name 6)
    """
    out_graph = copy_graph(graph)
    out_graph.remove((mk_dummy('3'), FOAF.name, mk_dummy('4')))
    out_graph.add((mk_dummy('5'), FOAF.name, mk_dummy('6')))
    out_graph.add((mk_dummy('4'), FOAF.name, mk_dummy('5')))  # should do nothing already there

    return out_graph


def test_generate_patch_decorator():
    """
    Tests the generate_patch decorator for a simple function

    runs through generate_path_from_graph and apply and revert_patch

    # maybe in the future split these tests
    """
    graph = init_graph()
    # del (3, name, 4) and add two triples (5 name 6), (4 name 5)
    patch, out_g = simple_function(graph)

    print(patch.model_dump())
    assert isinstance(out_g, rdflib.Graph)
    assert isinstance(patch, RDFPatch)
    assert isinstance(patch.add_triples, rdflib.Graph)

    # assert ('A', mk_dummy('5'), FOAF.name, mk_dummy('6')) in patch
    # assert ('D', mk_dummy('3'), FOAF.name, mk_dummy('4')) in patch
    # print(patch.add_triples)
    print(list(patch.add_triples))  # .triples((None, None, None))))
    # print([(s, p, o) for s, p, o in patch.add_triples])
    print(type(patch.add_triples))
    assert patch.add_triples is not None
    print(patch.add_triples.serialize())
    assert (mk_dummy('5'), FOAF.name, mk_dummy('6')) in patch.add_triples
    triple = (mk_dummy('3'), FOAF.name, mk_dummy('4'))
    assert triple in patch.delete_triples  # pylint: disable unsupported-membership-test

    # header there?
    # other statements not there

    # test if one can apply the patch to the original graph and get the end graph
    graph = init_graph()

    o_graph = patch.apply(graph)

    assert isinstance(o_graph, rdflib.Graph)
    assert len(o_graph) == len(out_g)
    assert isomorphic(o_graph, out_g)

    # test if round turn through rever patch works
    i_graph = patch.revert(o_graph)
    # print([(s,p,o) for s,p,o in i_graph])
    # print([(s,p,o) for s,p,o in graph])

    assert isinstance(i_graph, rdflib.Graph)
    assert len(i_graph) > 0
    assert len(i_graph) == len(graph)
    assert isomorphic(i_graph, graph)


def test_de_serialization():
    """
    Test if a Patch serialized to disk and read back in is the same again
    """
    graph = init_graph()

    patch, out_g = simple_function(graph)

    json_dump = patch.model_dump()

    patch2 = RDFPatch(**json_dump)

    print('orig', patch.model_dump_json(indent=2))
    print('parsed', patch2.model_dump_json(indent=2))
    # assert patch2 == patch  # Somehow this is not the case, why?
    assert patch2.model_dump() == json_dump
