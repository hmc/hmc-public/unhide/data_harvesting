# -*- coding: utf-8 -*-
"""
Tests for the command line interface (CLI)
"""
from typer.testing import CliRunner

from data_harvesting.cli.cli import cli

import pytest

runner = CliRunner()


def test_cli():
    """
    Test if the main cli command is available
    """
    result = runner.invoke(cli, ['--help'])
    assert result.exit_code == 0


def test_cli_aggregator():
    """
    Test the aggregator sub command group
    """
    result = runner.invoke(cli, ['aggregator', '--help'])
    assert result.exit_code == 0
    result = runner.invoke(cli, ['aggregator', 'uplift', '--help'])
    assert result.exit_code == 0


def test_cli_cron_available():
    """Check that cron is available."""
    result = runner.invoke(cli, ['cron', '--help'])
    assert result.exit_code == 0


@pytest.mark.skip(reason='fix this once figured out why this fails on the CI')
def test_cli_cron_commands():
    result = runner.invoke(cli, ['cron', 'list'])
    assert result.exit_code == 0
    assert 'Crontab' in result.stdout
    assert 'Anacron' in result.stdout
    result = runner.invoke(cli, ['cron', 'remove', '--help'])
    assert result.exit_code == 0
    result = runner.invoke(cli, ['cron', 'setup', '--help'])
    assert result.exit_code == 0


def test_cli_dcxml():
    """
    Test the cron sub command group
    """
    result = runner.invoke(cli, ['dcxml', '--help'])
    assert result.exit_code == 0
    result = runner.invoke(cli, ['dcxml', 'convert', '--help'])
    assert result.exit_code == 0


def test_cli_harvester():
    """
    Test the harvester sub command group
    """
    result = runner.invoke(cli, ['harvester', 'list'])
    assert result.exit_code == 0
    assert 'git' in result.stdout
    assert 'sitemap' in result.stdout
    result = runner.invoke(cli, ['harvester', 'run', '--help'])
    assert result.exit_code == 0


def test_cli_indexer():
    """
    Test the indexer sub command group
    """
    result = runner.invoke(cli, ['indexer', '--help'])
    assert result.exit_code == 0
    result = runner.invoke(cli, ['indexer', 'index', '--help'])
    assert result.exit_code == 0


def test_cli_rdf():
    """
    Test the rdf sub command group
    """
    result = runner.invoke(cli, ['rdf', 'convert', '--help'])
    assert result.exit_code == 0
    result = runner.invoke(cli, ['rdf', 'convert-unhide', '--help'])
    assert result.exit_code == 0
    result = runner.invoke(cli, ['rdf', 'diff', '--help'])
    assert result.exit_code == 0
    result = runner.invoke(cli, ['rdf', 'validate', '--help'])
    assert result.exit_code == 0


def test_cli_util():
    """
    Test if the util sub command group is there responsive
    """

    result = runner.invoke(cli, ['util', '--help'])
    assert result.exit_code == 0
    result = runner.invoke(cli, ['util', 'correct-keywords', '--help'])
    assert result.exit_code == 0
    result = runner.invoke(cli, ['util', 'upload', '--help'])
    assert result.exit_code == 0


def test_cli_pipeline():
    """
    test if the pipeline cli is available and shows help
    """
    result = runner.invoke(cli, ['pipeline', '--help'])
    assert result.exit_code == 0
