This is a test meant to test the pipeline for each harvester an resource type.
Be sure that prefect is running. if not start it with 'prefect server start'.
run this by executing

```
hmc-unhide pipeline run --config config_test.yaml --no-index --no-upload
```
