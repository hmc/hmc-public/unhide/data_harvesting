# -*- coding: utf-8 -*-
from unittest.mock import patch
from data_harvesting.stats.report import get_report_item
from data_harvesting.stats.models import ReportItem

TEST_REPORT_ITEMS = [
    ReportItem(name='Test Item 1', domain_name='test.com', url_patterns=[r'^\/$']),
    ReportItem(name='Test Item 2', domain_name='abc.test.com', url_patterns=[r'^\/$', r'^\/test*']),
]


@patch('data_harvesting.stats.report.REPORT_ITEMS', TEST_REPORT_ITEMS)
def test_get_report_item():
    assert get_report_item('test.com', '/').name == 'Test Item 1'
    assert get_report_item('abc.test.com', '/test').name == 'Test Item 2'
    assert get_report_item('test.com', '/test') is None
    assert get_report_item('abc.test.com', '/').name == 'Test Item 2'
