# -*- coding: utf-8 -*-
from unittest.mock import patch, call
from data_harvesting.stats.report.flow import print_rows, save_csv, REPORT_FILE
from data_harvesting.stats.models import LogData
from datetime import datetime
from prefect import flow


def test_print_rows():
    rows = [
        LogData('GET', 'https://example.com', 200, 1716896744, 'example.com', 'Index Page'),
        LogData('POST', 'https://example.com/api', 404, 1716896844, 'example.com', 'API Page'),
        LogData('GET', 'https://example.com/about', 200, 1716896944, 'example.com', 'About Page'),
    ]

    with patch('data_harvesting.stats.report.flow.get_run_logger') as mock_logger:
        mock_debug = mock_logger.return_value.debug

        print_rows(rows)

        mock_debug.assert_has_calls(
            [
                call('Report Item: Index Page\n'),
                call('Request Method | Request URL | Status Code | Request Time | Domain Name'),
                call('-' * 100),
                call('GET | https://example.com | 200 | 1716896744 | example.com'),
                call('POST | https://example.com/api | 404 | 1716896844 | example.com'),
                call('GET | https://example.com/about | 200 | 1716896944 | example.com'),
                call('\n\n'),
            ]
        )


def test_print_rows_empty():
    rows = []

    with patch('data_harvesting.stats.report.flow.get_run_logger') as mock_logger:
        print_rows(rows)

        mock_logger.assert_not_called()


def test_save_csv_valid():
    """Test saving valid data to a CSV file."""
    data = [('Item1', 10), ('Item2', 20)]
    test_time = datetime(2023, 1, 1)

    # create the folder of the CSV file
    REPORT_FILE.parent.mkdir(parents=True, exist_ok=True)

    @flow(name='test_flow_valid_csv')
    def save_csv_fn(data, time):
        save_csv(data, time)

    save_csv_fn._run(data=data, time=test_time)

    assert REPORT_FILE.is_file(), 'CSV file was not created.'

    with open(REPORT_FILE, 'r') as f:
        content = f.read()

    expected_content = 'Date,Item1,Item2\n2023-01-01,10,20\n'
    assert content == expected_content, f'CSV content mismatch. Got: {content}'

    REPORT_FILE.unlink()


def test_save_csv_no_data():
    """Test handling of no data scenario."""

    @flow(name='test_flow_no_data')
    def save_csv_fn():
        save_csv([])

    with patch('data_harvesting.stats.report.flow.get_run_logger') as mock_logger:
        save_csv_fn._run()
        mock_logger().info.assert_called_with('No data to save.')

    assert not REPORT_FILE.is_file(), 'CSV file should not be created for empty data.'


def test_save_csv_invalid_data_type():
    """Test handling of invalid data type."""
    data = 'invalid data type'

    @flow(name='test_flow_invalid_data_type')
    def save_csv_fn(data):
        save_csv(data)

    with patch('data_harvesting.stats.report.flow.get_run_logger') as mock_logger:
        save_csv_fn._run(data)
        mock_logger().info.assert_called_with('No data to save.')

    assert not REPORT_FILE.is_file(), 'CSV file should not be created for invalid data type.'


def test_save_csv_overwrite_existing():
    """Test overwriting an existing CSV file."""
    # create the folder of the CSV file
    REPORT_FILE.parent.mkdir(parents=True, exist_ok=True)

    with open(REPORT_FILE, 'w') as f:
        f.write('Date,ExistingItem\n2022-12-31,5\n')

    data = [('Item1', 10)]
    test_time = datetime(2023, 1, 1)

    @flow(name='test_flow_overwrite_existing')
    def save_csv_fn(data, time):
        save_csv(data, time)

    save_csv_fn._run(data, test_time)
    assert REPORT_FILE.is_file(), 'CSV file was not created.'

    with open(REPORT_FILE, 'r') as f:
        content = f.read()

    expected_content = 'Date,ExistingItem\n2022-12-31,5\n2023-01-01,10\n'
    assert content == expected_content, f'CSV content mismatch. Got: {content}'

    REPORT_FILE.unlink()
