# -*- coding: utf-8 -*-
from unittest.mock import patch, MagicMock, ANY
from data_harvesting.stats.config import GIT_FOLDER
from data_harvesting.stats.report.git import clone_repository, commit_and_push
from prefect import flow

TEST_REPOSITORY_URL = 'https://test.com/asd.git'
TEST_REPOSITORY_BRANCH = 'main'
TEST_GIT_USERNAME = 'test_user'
TEST_GIT_EMAIL = 'test_password'
TEST_GIT_TOKEN = 'test_token'


@patch('data_harvesting.stats.report.git.REPOSITORY_URL', None)
def test_clone_repository_no_url(caplog):
    @flow(name='test_clone_repository_no_url')
    def clone_repository_fn():
        clone_repository()

    clone_repository_fn._run()
    assert 'Repository URL is not set.' in caplog.text


@patch('data_harvesting.stats.report.git.REPOSITORY_URL', TEST_REPOSITORY_URL)
@patch('data_harvesting.stats.report.git.GIT_USERNAME', '')
@patch('data_harvesting.stats.report.git.GIT_EMAIL', '')
@patch('data_harvesting.stats.report.git.GIT_TOKEN', '')
def test_clone_repository_no_credentials(caplog):
    @flow(name='test_clone_repository_no_credentials')
    def clone_repository_fn():
        clone_repository()

    clone_repository_fn._run()
    assert 'Git credentials are not set.' in caplog.text


@patch('data_harvesting.stats.report.git.REPOSITORY_URL', TEST_REPOSITORY_URL)
@patch('data_harvesting.stats.report.git.REPOSITORY_BRANCH', TEST_REPOSITORY_BRANCH)
@patch('data_harvesting.stats.report.git.GIT_USERNAME', TEST_GIT_USERNAME)
@patch('data_harvesting.stats.report.git.GIT_TOKEN', TEST_GIT_TOKEN)
def test_clone_repository_success():
    @flow(name='test_clone_repository_success')
    def clone_repository_fn():
        clone_repository()

    with patch('pygit2.clone_repository') as mock_clone_repository:
        clone_repository_fn._run()
        mock_clone_repository.assert_called_once_with(
            TEST_REPOSITORY_URL,
            GIT_FOLDER,
            checkout_branch=TEST_REPOSITORY_BRANCH,
            callbacks=ANY,
        )


@patch('data_harvesting.stats.report.git.GIT_USERNAME', '')
@patch('data_harvesting.stats.report.git.GIT_EMAIL', '')
@patch('data_harvesting.stats.report.git.GIT_TOKEN', '')
def test_commit_and_push_no_credentials(caplog):
    @flow(name='test_commit_and_push_no_credentials')
    def commit_and_push_fn():
        commit_and_push()

    commit_and_push_fn._run()
    assert 'Git credentials are not set.' in caplog.text


@patch('data_harvesting.stats.report.git.GIT_USERNAME', TEST_GIT_USERNAME)
@patch('data_harvesting.stats.report.git.GIT_TOKEN', TEST_GIT_TOKEN)
@patch('data_harvesting.stats.report.git.GIT_EMAIL', TEST_GIT_EMAIL)
@patch('data_harvesting.stats.report.git.REPOSITORY_BRANCH', TEST_REPOSITORY_BRANCH)
def test_commit_and_push_success():
    @flow(name='test_commit_and_push_success')
    def commit_and_push_fn():
        commit_and_push()

    with patch('pygit2.Repository') as mock_repository:
        repo = MagicMock()
        mock_repository.return_value = repo
        repo.head.shorthand = TEST_REPOSITORY_BRANCH  # Correctly setting the branch name

        commit_and_push_fn._run()
        repo.index.add_all.assert_called_once()
        repo.index.write.assert_called_once()
        repo.create_commit.assert_called_once()
        repo.remotes['origin'].push.assert_called_once()
