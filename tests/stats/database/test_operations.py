# -*- coding: utf-8 -*-
from unittest.mock import patch
from data_harvesting.stats.models import LogData
from data_harvesting.stats.database.setup import create_db
from data_harvesting.stats.database.operations import insert_log, get_weekly_data, get_last_request_time
from datetime import datetime
from pathlib import Path

TEST_DB_NAME = Path('test.db')


@patch('data_harvesting.stats.database.setup.DB_NAME', TEST_DB_NAME)
@patch('data_harvesting.stats.database.operations.DB_NAME', TEST_DB_NAME)
def test_insert_log(db_connection):
    # Create the database
    create_db()

    test_data = LogData('GET', '/', 200, 1620000000, 'example.com', 'Index Page')

    # Insert the log into the database
    insert_log(test_data)

    # Check if the log was inserted into the database
    cursor = db_connection.cursor()
    cursor.execute('SELECT * FROM access_logs')
    result = cursor.fetchone()

    # Assert that the log was inserted into the database
    assert result == ('GET', '/', 200, 1620000000, 'example.com', 'Index Page')


@patch('data_harvesting.stats.database.setup.DB_NAME', TEST_DB_NAME)
@patch('data_harvesting.stats.database.operations.DB_NAME', TEST_DB_NAME)
def test_get_last_request_time(db_connection):
    # Create the database
    create_db()

    test_data = LogData('GET', '/', 200, 1620000000, 'example.com', 'Index Page')
    test_data2 = LogData('GET', '/', 301, 1620000002, 'example.com', 'Index Page')

    # Insert the log into the database
    insert_log(test_data)
    insert_log(test_data2)

    # Check if the log was inserted into the database
    result = get_last_request_time()

    # Assert that the log was inserted into the database
    assert result == 1620000002


@patch('data_harvesting.stats.database.setup.DB_NAME', TEST_DB_NAME)
@patch('data_harvesting.stats.database.operations.DB_NAME', TEST_DB_NAME)
def test_get_weekly_data(db_connection):
    # Create the database
    create_db()

    test_data = LogData('GET', '/', 200, 1620000000, 'example.com', 'Index Page')
    test_data2 = LogData('GET', '/', 301, 1620000002, 'example.com', 'Index Page')
    test_data3 = LogData('GET', '/', 404, 1620000004, 'example.com', 'Index Page')
    test_data4 = LogData('GET', '/search', 200, 1620000003, 'example.com', 'Search Page')
    test_data5 = LogData('GET', '/search', 200, 1620000005, 'example.com', 'Search Page')

    # Insert the log into the database
    insert_log(test_data)
    insert_log(test_data2)
    insert_log(test_data3)
    insert_log(test_data4)
    insert_log(test_data5)

    time = datetime.fromtimestamp(1620000005)

    # check for Index Page
    result = get_weekly_data('Index Page', time)
    assert len(result) == 3

    # check for Search Page
    result = get_weekly_data('Search Page', time)
    assert len(result) == 2

    # check for non-existent report item
    result = get_weekly_data('Non-existent Page', time)
    assert len(result) == 0

    # change time to 1 second before the last log
    time = datetime.fromtimestamp(1620000003)

    # check for Index Page
    result = get_weekly_data('Index Page', time)
    assert len(result) == 2

    # check for Search Page
    result = get_weekly_data('Search Page', time)
    assert len(result) == 1
