# -*- coding: utf-8 -*-
from data_harvesting.stats.database.setup import create_db
from unittest.mock import patch
from pathlib import Path


@patch('data_harvesting.stats.database.setup.DB_NAME', Path('test.db'))
def test_create_db(db_connection):
    create_db()

    # Check if the table exists in the database
    cursor = db_connection.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='access_logs'")
    result = cursor.fetchone()

    # Assert that the table exists
    assert result is not None
