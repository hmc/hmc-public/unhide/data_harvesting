# -*- coding: utf-8 -*-
import sqlite3
import pytest
from os import remove
from prefect.testing.utilities import prefect_test_harness


@pytest.fixture(autouse=True, scope='session')
def prefect_test_fixture():
    with prefect_test_harness():
        yield


@pytest.fixture
def db_connection():
    """Create a database connection for testing.

    Usage:
        @patch('data_harvesting.stats.database.setup.DB_NAME', 'test.db')
        def test_create_db(db_connection):
            ...

    Yields:
        sqlite3.Connection: A connection to the database.
    """
    # Create the database
    db_name = 'test.db'
    conn = sqlite3.connect(db_name)
    yield conn

    # Close the connection
    conn.close()

    # Remove the database file
    remove(db_name)
