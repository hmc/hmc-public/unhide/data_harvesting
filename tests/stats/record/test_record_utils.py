# -*- coding: utf-8 -*-
from unittest.mock import patch
from data_harvesting.stats.record.utils import parse_log_line
from data_harvesting.stats.models import LogData, ReportItem
from datetime import datetime


@patch('data_harvesting.stats.record.utils.get_report_item')
def test_parse_log_line(mock_get_report_item):
    log_line = """search.unhide.helmholtz-metadaten.de 127.0.0.1 - - [25/May/2024:12:34:56 +0000] "GET / HTTP/1.1" 200 1024 "http://search.unhide.helmholtz-metadaten.de" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36" "127.0.0.2:8080" """

    mock_get_report_item.return_value = None

    # if report_item is None, the function should return None
    assert parse_log_line(log_line) is None

    mock_get_report_item.return_value = ReportItem(
        name='report_item', domain_name='search.unhide.helmholtz-metadaten.de', url_patterns=['/']
    )

    # if report_item is not None, the function should return a LogData object
    request_time = datetime.strptime('25/May/2024:12:34:56 +0000', '%d/%b/%Y:%H:%M:%S %z').timestamp()
    expected = LogData(
        request_method='GET',
        request_url='/',
        status_code=200,
        request_time=request_time,
        domain_name='search.unhide.helmholtz-metadaten.de',
        report_item_name='report_item',
    )
    assert parse_log_line(log_line) == expected
