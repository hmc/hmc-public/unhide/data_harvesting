# -*- coding: utf-8 -*-
from unittest.mock import patch
from data_harvesting.stats.record.flow import save_logs
from data_harvesting.stats.database.setup import create_db
from pathlib import Path

TEST_DB_NAME = Path('test.db')


@patch('data_harvesting.stats.database.setup.DB_NAME', TEST_DB_NAME)
@patch('data_harvesting.stats.database.operations.DB_NAME', TEST_DB_NAME)
@patch('data_harvesting.stats.record.flow.LOG_PATH', 'tests/stats/record/access.log')
def test_save_logs(db_connection):
    # Create the database
    create_db()

    # run the flow
    save_logs()

    # check if log data was saved to the database
    log_data = db_connection.execute('SELECT * FROM access_logs').fetchall()

    assert len(log_data) == 10
    assert len([log for log in log_data if log[2] == 200]) == 7
    assert len([log for log in log_data if log[5].startswith('Search')]) == 5
    assert len([log for log in log_data if log[5].startswith('Sparql')]) == 5
