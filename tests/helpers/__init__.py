# -*- coding: utf-8 -*-
"""Helper utitilies for test suite."""
from pathlib import Path

import pytest


TEST_DIR = Path(__file__).resolve().parent.parent
"""Project test directory."""

TEST_DATA_DIR = TEST_DIR / 'data'
"""Project test data directory."""

PROJECT_ROOT_DIR = TEST_DIR.parent
"""Project root directory."""


def sorted_json(obj):
    """Return an ordered structure from a json-like object (i.e. with sorted objects and arrays)."""
    if isinstance(obj, dict):
        return sorted((k, sorted_json(v)) for k, v in obj.items())
    if isinstance(obj, list):
        return sorted(sorted_json(x) for x in obj)
    return obj


filter_prefect_mark = pytest.mark.filterwarnings('ignore::pydantic.warnings.PydanticDeprecatedSince20', 'ignore::sqlalchemy.exc.SAWarning')


def filter_prefect_warnings(func):
    """Filter warnings related to prefect."""
    # TODO: check if prefect has resolved these issues, then this decorator is not needed anymore.
    return filter_prefect_mark(func)
