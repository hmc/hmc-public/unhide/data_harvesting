# -*- coding: utf-8 -*-
"""
Configurations and fixtures for data_harvesting tests
"""
from pathlib import Path
import pytest
from helpers import TEST_DATA_DIR


print(f'Global test data directory: {TEST_DATA_DIR}')


@pytest.fixture
def example_inputs(tmp_path_factory):
    """Return path for an xml example file in test directory, based on file name.

    If copy is True, will return a copy of desired file inside a auto-cleaned up tempdir.
    """

    def _get_input(filename: str, *, copy: bool = False) -> Path:
        filepath = TEST_DATA_DIR / filename
        if not copy:
            return filepath

        copypath: Path = tmp_path_factory.mktemp('example_input') / filename
        copypath.write_bytes(filepath.read_bytes())
        return copypath

    yield _get_input
