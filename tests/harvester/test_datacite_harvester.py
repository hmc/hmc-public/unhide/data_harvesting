# -*- coding: utf-8 -*-
"""Tests for the datacite pipeline"""

import yaml

from data_harvesting.harvester.datacite import correct_keywords
from data_harvesting.harvester.datacite import DataciteHarvester
from data_harvesting.harvester.datacite import extract_metadata_restapi
from data_harvesting.harvester.datacite import query_graphql_api

text_graphql = """{
  "data": {
    "organization": {
      "id": "https://ror.org/02nv7yv05",
      "name": "Forschungszentrum Jülich",
      "works": {
        "totalCount": 1501,
        "pageInfo": {
          "endCursor": "MTQ5OTQzNDUxMTAwMCwxMC41MDYxL2RyeWFkLmY5djgw",
          "hasNextPage": true
        },
        "nodes": [
          {
            "id": "https://doi.org/10.5061/dryad.7m645",
            "doi": "10.5061/dryad.7m645",
            "publisher": "Dryad",
            "relatedIdentifiers": [
              {
                "relatedIdentifier": "10.1098/rsos.140410",
                "relationType": "IsCitedBy"
              }
            ]
          },
          {
            "id": "https://doi.org/10.5061/dryad.m5sd2",
            "doi": "10.5061/dryad.m5sd2",
            "publisher": "Dryad",
            "relatedIdentifiers": [
              {
                "relatedIdentifier": "10.1098/rsos.160896",
                "relationType": "IsCitedBy"
              }
            ]
          },
          {
            "id": "https://doi.org/10.5061/dryad.f9v80",
            "doi": "10.5061/dryad.f9v80",
            "publisher": "Dryad",
            "relatedIdentifiers": [
              {
                "relatedIdentifier": "10.1021/jacs.7b05576",
                "relationType": "IsCitedBy"
              }
            ]
          }
        ]
      }
    }
  }
}"""

data_jsonld = {
    '@context': 'http://schema.org',
    '@type': 'Dataset',
    '@id': 'https://doi.org/10.5061/dryad.7m645',
    'url': 'http://datadryad.org/stash/dataset/doi:10.5061/dryad.7m645',
    'additionalType': 'dataset',
    'name': 'Data from: Information use by humans during dynamic route choice in virtual crowd evacuations',
    'author': [
        {
            'name': 'Nikolai W. F. Bode',
            'givenName': 'Nikolai W. F.',
            'familyName': 'Bode',
            'affiliation': {'@type': 'Organization', '@id': 'https://ror.org/0524sp257', 'name': 'University of Bristol'},
            '@type': 'Person',
        },
        {
            'name': 'Armel U. Kemloh Wagoum',
            'givenName': 'Armel U.',
            'familyName': 'Kemloh Wagoum',
            'affiliation': {'@type': 'Organization', '@id': 'https://ror.org/02nv7yv05', 'name': 'Forschungszentrum Jülich'},
            '@type': 'Person',
        },
        {
            'name': 'Edward A. Codling',
            'givenName': 'Edward A.',
            'familyName': 'Codling',
            'affiliation': {'@type': 'Organization', '@id': 'https://ror.org/02nkf1q06', 'name': 'University of Essex'},
            '@type': 'Person',
        },
    ],
    'description': 'We conducted a computer-based experiment with over 450 human participants and used a Bayesian model selection approach to explore dynamic exit route choice mechanisms of individuals in simulated crowd evacuations. In contrast to previous work, we explicitly explore the use of time-dependent and time-independent information in decision-making. Our findings suggest that participants tended to base their exit choices on time-dependent information, such as differences in queue lengths and queue speeds at exits rather than on time-independent information, such as differences in exit widths or exit route length. We found weak support for similar decision-making mechanisms under a stress-inducing experimental treatment. However, under this treatment participants were less able or willing to adjust their original exit choice in the course of the evacuation. Our experiment is not a direct test of behaviour in real evacuations, but it does highlight the role different types of information and stress play in real human decision-making in a virtual environment. Our findings may be useful in identifying topics for future study on real human crowd movements or for developing more realistic agent-based simulations.',
    'license': 'https://creativecommons.org/publicdomain/zero/1.0/legalcode',
    'version': '1',
    'keywords': 'Bayesian model selection, crowd evacuations, decision making, virtual environment, route choice',
    'inLanguage': 'en',
    'contentSize': '812634198 bytes',
    'datePublished': '2015',
    'schemaVersion': 'http://datacite.org/schema/kernel-4',
    'publisher': {'@type': 'Organization', 'name': 'Dryad'},
    'provider': {'@type': 'Organization', 'name': 'datacite'},
}


def test_query_graphql_api(requests_mock):
    """
    Test to query the graphlql api of datacite
    We query the real thing here! maybe at least do not run this on the CI
    """
    # query = '{organization(id: "https://ror.org/02nv7yv05") {id name works (first:3){totalCount pageInfo {endCursor hasNextPage} nodes {id doi publisher relatedIdentifiers{relatedIdentifier relationType}}}}}'

    url = 'https://api.datacite.org/graphql'

    requests_mock.post(url, text=text_graphql)
    roar = 'https://ror.org/02nv7yv05'  # FZJ
    pid_list, json_data_all = query_graphql_api(roar, max_count=3, get_all=False)

    assert len(pid_list) == 3


def test_extract_metadata_restapi(requests_mock):
    """
    Test extract metadata of a single pid from datacite
    """

    url = 'https://api.datacite.org/dois/10.5061/dryad.7m645'

    requests_mock.get(url, json=data_jsonld, status_code=200)

    pid = 'https://doi.org/10.5061/dryad.7m645'
    data_res = extract_metadata_restapi(pid)
    assert data_jsonld == data_res


def test_dataciteharvester():
    """
    Basic test for DataciteHarvester init and roars
    """

    dch = DataciteHarvester()
    # roars = dch.get_rors()
    roars = dch.sources
    print(roars)
    # maybe do this with a custom config...
    assert 'FZJ' in roars


def test_correct_keywords():
    """
    Test if keywords are corrected corretly.
    """
    data_in = {
        'keywords': 'DEPTH, sediment/rock, Carbon, inorganic, total, Calcium carbonate, Carbon, total, Carbon, organic, total, Nitrogen, total, Sulfur, total, Porosity, volume, Push corer, Element analyser CNS, Carlo Erba NA1500, SO242/2, Sonne_2, JPI Oceans - Ecological Aspects of Deep-Sea Mining (JPIO-MiningImpact)',
        'provider': {'@type': 'Organization', 'name': 'datacite'},
    }

    data_should = {
        'keywords': [
            'DEPTH',
            'sediment/rock',
            'Carbon',
            'inorganic',
            'total',
            'Calcium carbonate',
            'Carbon',
            'total',
            'Carbon',
            'organic',
            'total',
            'Nitrogen',
            'total',
            'Sulfur',
            'total',
            'Porosity',
            'volume',
            'Push corer',
            'Element analyser CNS',
            'Carlo Erba NA1500',
            'SO242/2',
            'Sonne_2',
            'JPI Oceans - Ecological Aspects of Deep-Sea Mining (JPIO-MiningImpact)',
        ],
        'provider': {'@type': 'Organization', 'name': 'datacite'},
    }

    corrected = correct_keywords(data_in)

    assert data_should == corrected


config_str = """
DataciteHarvester:
  follow_children: true
  sources:
    FZJ:
      name: FZJ
      ror: https://ror.org/02nv7yv05
"""

config = yaml.safe_load(config_str)


def test_dataciteharvester_full(requests_mock, tmp_path):
    """
    Full regression test for DataciteHarvester
    """

    # because of laysiness we mock all urls with the same jsonld data
    url1 = 'https://api.datacite.org/dois/10.5061/dryad.7m645'
    url2 = 'https://api.datacite.org/dois/10.5061/dryad.m5sd2'
    url3 = 'https://api.datacite.org/dois/10.5061/dryad.f9v80'

    requests_mock.get(url1, json=data_jsonld, status_code=200)
    requests_mock.get(url2, json=data_jsonld, status_code=200)

    requests_mock.get(url3, json=data_jsonld, status_code=400)  # failure
    # the failure should not cause the harvester to fail...

    url_graphql = 'https://api.datacite.org/graphql'
    requests_mock.post(url_graphql, text=text_graphql, status_code=200)

    print(tmp_path)
    dch = DataciteHarvester(outpath=tmp_path)

    dch.config = config.get(dch.__class__.__name__, {})
    dch.sources = dch.config.get('sources', {})

    dch.run()
