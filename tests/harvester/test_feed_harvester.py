# -*- coding: utf-8 -*-
"""Tests for the feed harvester"""

import yaml

from data_harvesting.harvester.feed import convert_micro_jsonld
from data_harvesting.harvester.feed import FeedHarvester

# mock two requests, one to get the feed and a second one to get the corresponding jsonld_data

config_str = """
FeedHarvester:
  sources:
    HIFIS:
      urls:
        - "https://events.hifis.net/category/150/events.atom"
"""

#    KIT:
#      urls:
#       - "https://indico.scc.kit.edu/category/1/events.atom"

config = yaml.safe_load(config_str)

feed = """<?xml version='1.0' encoding='UTF-8'?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <id>https://events.hifis.net/category/150/events.atom</id>
  <title>Indico Feed [Beyond Helmholtz]</title>
  <updated>2023-12-08T13:34:17.574907+00:00</updated>
  <link href="https://events.hifis.net/category/150/events.atom" rel="self"/>
  <generator uri="https://lkiesow.github.io/python-feedgen" version="0.9.0">python-feedgen</generator>
  <entry>
    <id>https://events.hifis.net/event/994/</id>
    <title>deRSE24 - Conference for Research Software Engineering in Germany</title>
    <updated>2024-03-05T12:00:00+00:00</updated>
    <link href="https://events.hifis.net/event/994/" rel="alternate"/>
    <summary type="html">&lt;h2&gt;Facts&lt;/h2&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;What:&lt;/strong&gt; 4th conference for Research Software Engineering in Germany&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Begin:&lt;/strong&gt; 05.03.24&lt;/li&gt;&lt;li&gt;&lt;strong&gt;End:&lt;/strong&gt; 07.03.24&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Organized and hosted by &lt;/strong&gt;&lt;a href="https://www.mathematik-informatik.uni-wuerzburg.de/sonstiges/anfahrt-und-lageplan/fakultaet-fuer-mathematik-und-informatik/naturwissenschaftlicher-hoersaalbau-p4/" target="_blank" rel="noopener noreferrer"&gt;Julius-Maximilians-Universität Würzburg&lt;/a&gt;&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Location&lt;/strong&gt;: Lecture Hall of the Natural Sciences(NWHS)&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;&lt;a href="https://www.mathematik-informatik.uni-wuerzburg.de/sonstiges/anfahrt-und-lageplan/fakultaet-fuer-mathematik-und-informatik/naturwissenschaftlicher-hoersaalbau-p4/" target="_blank" rel="noopener noreferrer"&gt;&lt;img class="image_resized" style="width:35.9%;" src="/event/932/attachments/1297/2292/Universit%C3%A4t_W%C3%BCrzburg_Logo.png"&gt;&lt;/a&gt;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Call for Contributions: &lt;/strong&gt;See &lt;a href="https://derse24.physik.uni-wuerzburg.de/abstracts"&gt;this link&lt;/a&gt; or the left side bar.&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Registration:&lt;/strong&gt; Will be available soon, stay tuned!&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Fee:&lt;/strong&gt; Planned to be around 220 € full price. Less for students and members&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Code of Conduct &amp;amp; Diversity Plan&lt;/strong&gt;: &lt;a href="https://events.hifis.net/event/994/page/183-code-of-conduct"&gt;See here&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;&lt;h3&gt;Important Dates&lt;/h3&gt;&lt;ul&gt;&lt;li&gt;&lt;strong&gt;Conference start:&lt;/strong&gt; Mar., 5th 2024&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Conference end:&lt;/strong&gt; Mar., 7th 2024&lt;/li&gt;&lt;li&gt;&lt;strong&gt;Contribution Deadline: Nov&lt;/strong&gt;., &lt;s&gt;12th&lt;/s&gt; 26th (CET) 2023&lt;/li&gt;&lt;/ul&gt;</summary>
  </entry>
</feed>""".encode('utf-8')


def test_feed_harvester(requests_mock, tmp_path, example_inputs):
    """Full regression test for the feed harvester"""
    content = example_inputs('event_page.html').read_text()

    requests_mock.get('https://events.hifis.net/category/150/events.atom', content=feed, status_code=200)
    requests_mock.get('https://events.hifis.net/event/994/', text=content, status_code=200)

    fh = FeedHarvester(outpath=tmp_path)
    fh.config = config.get(fh.__class__.__name__, {})
    fh.sources = fh.config.get('sources', {})

    assert not fh.get_last_run_file('HIFIS').is_file()
    fh.run(source='HIFIS')
    assert fh.get_last_run_file('HIFIS').is_file()

    fh.get_last_run_file('HIFIS').unlink(missing_ok=True)

    assert not fh.get_last_run_file().is_file()
    fh.run()  # now all instances
    assert fh.get_last_run_file().is_file()

    fh.get_last_run_file().unlink(missing_ok=True)


'''
config2_str = """
FeedHarvester:
  sources:
    HIFIS:
"""
config2 = yaml.safe_load(config2_str)

def test_non_crash_feed_harvester_empty_conf():
    """
    """
    fh = FeedHarvester()
    fh.config = config2.get(fh.__class__.__name__, {})
    fh.sources = fh.config.get('sources', {})
    fh.run(instance='HIFIS')

    fh.run() # now all instances
'''


def test_convert_micro_jsonld():
    """test convert_micro_jsonld"""

    microdata = [{'properties': {'description': '', 'title': 'HUF 2024'}, 'type': 'http://schema.org/Event'}]
    json_ld_should = {'@context': 'https://schema.org', '@type': 'Event', 'title': 'HUF 2024', 'description': ''}

    json_ld_is = convert_micro_jsonld(microdata)
    assert json_ld_is == json_ld_should

    microdata = []
    json_ld_should = {'@context': 'https://schema.org'}

    json_ld_is = convert_micro_jsonld(microdata)
    assert json_ld_is == json_ld_should
