# -*- coding: utf-8 -*-
"""
Tests concerning the sitemap harvester pipeline
"""
from datetime import datetime

import pandas as pd
import pytest
import yaml

from data_harvesting.harvester.sitemap import filter_urls
from data_harvesting.harvester import SitemapHarvester
from data_harvesting.harvester.sitemap import transform_url
from helpers import TEST_DATA_DIR


def test_filter_urls():
    """
    Test if all filters for the respective sitemaps work respectively

    For this load stored sitemaps dfs and apply respective filters
    """
    # todo in the future parameterize this test?

    # rodare
    df_path = TEST_DATA_DIR / 'rodare_sitemap_df.csv'
    match_pattern = r'.*/record/\d'
    check_pattern = '/record/'

    sitemap_df = pd.read_csv(df_path)
    urls = sitemap_df

    new_urls = filter_urls(urls, match_pattern=match_pattern)

    assert len(new_urls['loc']) > 0

    for url in new_urls['loc']:
        assert check_pattern in url

    # antipattern does not work yet
    new_urls = filter_urls(urls, antimatch_pattern=match_pattern)

    for url in new_urls['loc']:
        assert check_pattern not in url

    new_urls = filter_urls(urls, match_pattern=match_pattern, antimatch_pattern=match_pattern)

    assert len(new_urls['loc']) == 0

    since = datetime.fromisoformat('2021-06-14')

    # juelich_data
    df_path = TEST_DATA_DIR / 'juelichdata_sitemap_df.csv'
    match_pattern = r'.*/dataset\.xhtml.*'
    check_pattern = '/dataset.xhtml'

    sitemap_df = pd.read_csv(df_path)
    urls = sitemap_df

    new_urls = filter_urls(urls, since=since, match_pattern=match_pattern)

    assert len(new_urls) > 0

    for url in new_urls['loc']:
        assert check_pattern in url

    for datet in new_urls['lastmod']:
        date_ = datetime.fromisoformat(datet).replace(tzinfo=None)  # .date()
        assert date_ >= since


def test_transform_url():
    """
    Test if the urls are transformed in the right way
    """
    urls_should = ['https://data.fz-juelich.de/api/datasets/export?exporter=schema.org&persistentId=doi:10.26165/JUELICH-DATA/VGEHRD']

    urls = ['https://data.fz-juelich.de/dataset.xhtml?persistentId=doi:10.26165/JUELICH-DATA/VGEHRD']
    transforms = [{'replace': ('dataset.xhtml?', 'api/datasets/export?exporter=schema.org&')}]

    new_urls = transform_url(urls, transforms)

    for i, new_url in enumerate(new_urls):
        assert new_url == urls_should[i]


config_str = """
SitemapHarvester:
  sources:
    FZJ:
    - name: Jülich Data
      url: https://data.fz-juelich.de/sitemap.xml
      match_pattern: '.*/dataset.xhtml.*'
      url_transforms:
        - replace: ["dataset.xhtml?", "api/datasets/export?exporter=schema.org&"]
"""

config = yaml.safe_load(config_str)

# instead of mocking the request or url lib call, we mock the function which calls the external library instead
'''
sitemap = """<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  <url>
    <loc>https://data.fz-juelich.de/dataset.xhtml?persistentId=doi:10.26165/JUELICH-DATA/FXM9CB</loc>
    <lastmod>2023-10-30</lastmod>
  </url>
</urlset>""".encode('utf-8')
'''


def mock_get_all_sitemaps(url: str) -> pd.DataFrame:
    """This function mocks data_harvesting.sitemap.sitemap_harvester.get_all_sitemaps for test purposes.
    not it returns a certain dataframe from disks
    """

    test_datafile = TEST_DATA_DIR / 'juelich_data_site9.csv'
    with open(test_datafile, 'r', encoding='utf-8') as fileo:
        dfr = pd.read_csv(fileo)
    return dfr


@pytest.fixture
def get_sm_patched(monkeypatch):
    import data_harvesting.harvester.sitemap

    monkeypatch.setattr(data_harvesting.harvester.sitemap, 'get_all_sitemaps', mock_get_all_sitemaps)


data_page = TEST_DATA_DIR / 'juelichdata_page.txt'
with open(data_page, 'r', encoding='utf-8') as fileo:
    content = fileo.read()

data_page2 = TEST_DATA_DIR / 'juelichdata_page2.txt'
with open(data_page2, 'r', encoding='utf-8') as fileo:
    content2 = fileo.read()


def test_sitemap_harvester(requests_mock, get_sm_patched, tmp_path):
    """Full regression test of sitemap harvester with mocked requests"""

    requests_mock.get(
        'https://data.fz-juelich.de/api/datasets/export?exporter=schema.org&persistentId=doi:10.26165/JUELICH-DATA/GCBNMA',
        text=content2,
        status_code=200,
    )
    requests_mock.get(
        'https://data.fz-juelich.de/api/datasets/export?exporter=schema.org&persistentId=doi:10.26165/JUELICH-DATA/WLCJ4X',
        text=content,
        status_code=200,
    )

    sh = SitemapHarvester(outpath=tmp_path)

    sh.config = config.get(sh.__class__.__name__, {})
    sh.sources = sh.config.get('sources', {})
    print(sh.config)
    print(sh.get_config())
    sh.run(source='FZJ')

    sh.run()  # now all instances
