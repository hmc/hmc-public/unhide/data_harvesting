# -*- coding: utf-8 -*-
"""Test the gitlab pipeline"""
import pytest

from data_harvesting.harvester.git import harvest_project


@pytest.mark.skip
def test_harvest_project():
    """Harvest a single public repository
    TODO this propertly maybe we mook codemeta-harvester
    """
    url = 'https://jugit.fz-juelich.de/c.schiffer/typhon'
    harvest_project(url, repo_url=url)
