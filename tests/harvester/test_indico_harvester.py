# -*- coding: utf-8 -*-
"""Tests for the feed harvester"""

import yaml

from data_harvesting.harvester.indico import IndicoHarvester

# mock two requests, one to get the feed and a second one to get the corresponding jsonld_data

config_str = """
IndicoHarvester:
  sources:
    HIFIS:
      categories: [150]
      url: https://events.hifis.net
      token: 'indp_3SicijJvemsUSbm18kl9dEDk2OmAPXK1h2r4OtJQAF'
"""

config = yaml.safe_load(config_str)

cat_json = {
    'count': 19,
    'results': [
        {
            '_type': 'Conference',
            'id': '994',
            'title': 'deRSE24 - Conference for Research Software Engineering in Germany',
            'description': '<h2>Facts</h2><ul><li><strong>What:</strong> 4th conference for Research Software Engineering in Germany</li><li><strong>Begin:</strong> 05.03.24</li><li><strong>End:</strong> 07.03.24</li><li><strong>Organized and hosted by </strong><a href="https://www.mathematik-informatik.uni-wuerzburg.de/sonstiges/anfahrt-und-lageplan/fakultaet-fuer-mathematik-und-informatik/naturwissenschaftlicher-hoersaalbau-p4/" target="_blank" rel="noopener noreferrer">Julius-Maximilians-Universität Würzburg</a></li><li><strong>Location</strong>: Lecture Hall of the Natural Sciences(NWHS)</li></ul><p><a href="https://www.mathematik-informatik.uni-wuerzburg.de/sonstiges/anfahrt-und-lageplan/fakultaet-fuer-mathematik-und-informatik/naturwissenschaftlicher-hoersaalbau-p4/" target="_blank" rel="noopener noreferrer"><img class="image_resized" style="width:35.9%;" src="/event/932/attachments/1297/2292/Universit%C3%A4t_W%C3%BCrzburg_Logo.png"></a></p><ul><li><strong>Call for Contributions: </strong>See <a href="https://derse24.physik.uni-wuerzburg.de/abstracts">this link</a> or the left side bar.</li><li><strong>Registration:</strong> Will be available soon, stay tuned!</li><li><strong>Fee:</strong> Planned to be around 220 € full price. Less for students and members</li><li><strong>Code of Conduct &amp; Diversity Plan</strong>: <a href="https://events.hifis.net/event/994/page/183-code-of-conduct">See here</a></li></ul><h3>Important Dates</h3><ul><li><strong>Conference start:</strong> Mar., 5th 2024</li><li><strong>Conference end:</strong> Mar., 7th 2024</li><li><strong>Contribution Deadline: Nov</strong>., <s>12th</s> 26th (CET) 2023</li></ul>',
            'startDate': {'date': '2024-03-05', 'time': '13:00:00', 'tz': 'Europe/Berlin'},
            'timezone': 'Europe/Berlin',
            'endDate': {'date': '2024-03-07', 'time': '13:00:00', 'tz': 'Europe/Berlin'},
            'room': 'Mathematisch-Naturwissenschaftliches Hörsaalgebäude',
            'location': 'Julius-Maximilians-Universität Würzburg',
            'address': 'Am Hubland\n97074 Würzburg',
            'type': 'conference',
            'references': [],
            '_fossil': 'conferenceMetadata',
            'categoryId': 151,
            'category': 'de-RSE',
            'note': {},
            'roomFullname': 'Mathematisch-Naturwissenschaftliches Hörsaalgebäude',
            'url': 'https://events.hifis.net/event/994/',
            'creationDate': {'date': '2023-09-11', 'time': '12:51:51.912755', 'tz': 'Europe/Berlin'},
            'creator': {
                '_type': 'Avatar',
                '_fossil': 'conferenceChairMetadata',
                'first_name': 'Tobias',
                'last_name': 'Huste',
                'fullName': 'Huste, Tobias',
                'id': '2',
                'affiliation': 'Helmholtz-Zentrum Dresden-Rossendorf',
                'emailHash': '391b8f3363111871aa8236c15d958b72',
            },
            'hasAnyProtection': False,
            'roomMapURL': None,
            'folders': [],
            'chairs': [],
            'material': [
                {
                    '_type': 'Material',
                    '_fossil': 'materialMetadata',
                    '_deprecated': True,
                    'title': 'Chair photos',
                    'id': '1349',
                    'resources': [
                        {
                            'name': 'anna_lena_lamprecht.jpg',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '2410',
                            'fileName': 'anna_lena_lamprecht.jpg',
                            'url': 'https://events.hifis.net/event/994/attachments/1349/2410/anna_lena_lamprecht.jpg',
                        },
                        {
                            'name': 'bernadette_fritzsch.jpg',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '2461',
                            'fileName': 'bernadette_fritzsch.jpg',
                            'url': 'https://events.hifis.net/event/994/attachments/1349/2461/bernadette_fritzsch.jpg',
                        },
                        {
                            'name': 'claire_wyatt.jpg',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '2414',
                            'fileName': 'claire_wyatt.jpg',
                            'url': 'https://events.hifis.net/event/994/attachments/1349/2414/claire_wyatt.jpg',
                        },
                        {
                            'name': 'Florian_Goth.jpg',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '2409',
                            'fileName': 'Florian_Goth.jpg',
                            'url': 'https://events.hifis.net/event/994/attachments/1349/2409/Florian_Goth.jpg',
                        },
                        {
                            'name': 'frank_loeffler.jpg',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '2412',
                            'fileName': 'frank_loeffler.jpg',
                            'url': 'https://events.hifis.net/event/994/attachments/1349/2412/frank_loeffler.jpg',
                        },
                        {
                            'name': 'frank_loeffler.jpg',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '2415',
                            'fileName': 'frank_loeffler.jpg',
                            'url': 'https://events.hifis.net/event/994/attachments/1349/2415/frank_loeffler.jpg',
                        },
                        {
                            'name': 'michele_mesiti.jpg',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '2413',
                            'fileName': 'michele_mesiti.jpg',
                            'url': 'https://events.hifis.net/event/994/attachments/1349/2413/michele_mesiti.jpg',
                        },
                        {
                            'name': 'stephan_janosch.jpg',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '2411',
                            'fileName': 'stephan_janosch.jpg',
                            'url': 'https://events.hifis.net/event/994/attachments/1349/2411/stephan_janosch.jpg',
                        },
                    ],
                },
                {
                    '_type': 'Material',
                    '_fossil': 'materialMetadata',
                    '_deprecated': True,
                    'title': 'Editor uploads',
                    'id': '1348',
                    'resources': [
                        {
                            'name': 'cropped-NHRatFAU-Logo-mSchriftzug.png',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '3194',
                            'fileName': 'cropped-NHRatFAU-Logo-mSchriftzug.png',
                            'url': 'https://events.hifis.net/event/994/attachments/1348/3194/cropped-NHRatFAU-Logo-mSchriftzug.png',
                        },
                        {
                            'name': 'cropped-NHRatFAU-Logo-mSchriftzug.png',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '3195',
                            'fileName': 'cropped-NHRatFAU-Logo-mSchriftzug.png',
                            'url': 'https://events.hifis.net/event/994/attachments/1348/3195/cropped-NHRatFAU-Logo-mSchriftzug.png',
                        },
                        {
                            'name': 'konwihr4x.png',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '2883',
                            'fileName': 'konwihr4x.png',
                            'url': 'https://events.hifis.net/event/994/attachments/1348/2883/konwihr4x.png',
                        },
                        {
                            'name': 'Universität_Würzburg_Logo.png',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '2408',
                            'fileName': 'Universität_Würzburg_Logo.png',
                            'url': 'https://events.hifis.net/event/994/attachments/1348/2408/Universit%C3%A4t_W%C3%BCrzburg_Logo.png',
                        },
                    ],
                },
                {
                    '_type': 'Material',
                    '_fossil': 'materialMetadata',
                    '_deprecated': True,
                    'title': 'web-assets',
                    'id': '1577',
                    'resources': [
                        {
                            'name': 'derse24-splash-website-colour.plain.svg',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '3022',
                            'fileName': 'derse24-splash-website-colour.plain.svg',
                            'url': 'https://events.hifis.net/event/994/attachments/1577/3022/derse24-splash-website-colour.plain.svg',
                        },
                        {
                            'name': 'Montserrat-Italic-VariableFont_wght.ttf',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '3023',
                            'fileName': 'Montserrat-Italic-VariableFont_wght.ttf',
                            'url': 'https://events.hifis.net/event/994/attachments/1577/3023/Montserrat-Italic-VariableFont_wght.ttf',
                        },
                        {
                            'name': 'Montserrat-Italic-VariableFont_wght.woff',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '3024',
                            'fileName': 'Montserrat-Italic-VariableFont_wght.woff',
                            'url': 'https://events.hifis.net/event/994/attachments/1577/3024/Montserrat-Italic-VariableFont_wght.woff',
                        },
                        {
                            'name': 'Montserrat-Italic-VariableFont_wght.woff2',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '3025',
                            'fileName': 'Montserrat-Italic-VariableFont_wght.woff2',
                            'url': 'https://events.hifis.net/event/994/attachments/1577/3025/Montserrat-Italic-VariableFont_wght.woff2',
                        },
                        {
                            'name': 'Montserrat-VariableFont_wght.ttf',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '3026',
                            'fileName': 'Montserrat-VariableFont_wght.ttf',
                            'url': 'https://events.hifis.net/event/994/attachments/1577/3026/Montserrat-VariableFont_wght.ttf',
                        },
                        {
                            'name': 'Montserrat-VariableFont_wght.woff',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '3027',
                            'fileName': 'Montserrat-VariableFont_wght.woff',
                            'url': 'https://events.hifis.net/event/994/attachments/1577/3027/Montserrat-VariableFont_wght.woff',
                        },
                        {
                            'name': 'Montserrat-VariableFont_wght.woff2',
                            '_deprecated': True,
                            '_type': 'LocalFile',
                            '_fossil': 'localFileMetadata',
                            'id': '3028',
                            'fileName': 'Montserrat-VariableFont_wght.woff2',
                            'url': 'https://events.hifis.net/event/994/attachments/1577/3028/Montserrat-VariableFont_wght.woff2',
                        },
                    ],
                },
            ],
            'keywords': [],
            'organizer': '',
            'language': None,
            'label': None,
            'visibility': {'id': '', 'name': 'Everywhere'},
        }
    ],
    'moreFutureEvents': False,
    'url': 'https://events.hifis.net/export/categ/150.json?from=2014-01-01&to=2034-01-01&pretty=yes',
    '_type': 'HTTPAPIResult',
}


def test_indico_harvester(requests_mock, tmp_path, example_inputs):
    """Full regression test for the indico harvester"""
    content = example_inputs('event_page.html').read_text()

    # Because of the current harvester since logic, this has to be changed each year plus one for now...
    # orginal query: 'https://events.hifis.net/export/categ/150.json?from=2014-01-01&to=2034-01-01&pretty=yes'
    # it might be if this fails, that the last run file has to be deleted
    requests_mock.get(
        'https://events.hifis.net/export/categ/150.json?from=2014-01-01&to=2034-01-01&pretty=yes', json=cat_json, status_code=200
    )
    # adapter = requests_mock.Adapter()
    # adapter.register_uri('GET', 'https://events.hifis.net/export/categ/150.json?from=20', json=cat_json, status_code=200)
    requests_mock.get('https://events.hifis.net/event/994/', text=content, status_code=200)

    fh = IndicoHarvester(outpath=tmp_path)
    fh.config = config.get(fh.__class__.__name__, {})
    fh.sources = fh.config.get('sources', {})

    fh.run(source='HIFIS', since=None)
    fh.run()  # now all instances
