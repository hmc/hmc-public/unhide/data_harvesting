# -*- coding: utf-8 -*-
import json
import pytest

from data_harvesting.util.external_schemas import cached_schema_path
from data_harvesting.util.rdf_util import Graph
from data_harvesting.data_model import LinkedDataObject
from data_harvesting.aggregator import Aggregator

from helpers import TEST_DATA_DIR

# comment out this line to run this benchmark module by default:
pytestmark = pytest.mark.skip(reason='benchmarks only are run on demand')
# ----


RDF_STORES = [None, 'Oxigraph']


@pytest.fixture(scope='function', params=RDF_STORES)
def rdf_stores(request):
    """Add this fixture to run a test using different RDF stores supported by rdflib.

    Note that this assumes all called functions are using the wrapped `Graph` class,
    instead of the regular `rdflib.Graph()` to instantiate a new graph.
    """
    import data_harvesting.util.rdf_util as ru

    def_store = ru.DEF_RDF_STORE
    ru.DEF_RDF_STORE = request.param
    yield
    ru.DEF_RDF_STORE = def_store


# The pure parsing of JSON is quite fast.
# ~14ms for the >1MB SHACL shape JSON-LD file!
# -> JSON parsing is not the bottleneck, using faster JSON parsers will not help.
@pytest.mark.skip()
def test_banchmark_parse_graph_json(benchmark):
    """Check speed of parsing a larger JSON file."""

    def load_file_json(path):
        f = open(path, 'r')
        json.load(f)

    benchmark(load_file_json, cached_schema_path('schema_org_shacl'))


# parsing a graph from JSON surprisingly seems to be fastest, NT is close, TTL is slower.
# parsing JSON from a dict via round-trip JSON serialization is about the same as JSON.
#
# Just parsing the JSON-LD file into a graph takes already ~1.4s !
# -> Re-parsing graphs should be avoided, reuse/caching is needed for performance.
@pytest.mark.skip()
@pytest.mark.parametrize('format', ('dict', 'json-ld', 'ttl', 'nt'))
def test_graph_parse(format, benchmark):
    """Compare speed between parsing triples from e.g. JSON-LD vs TTL vs NTriple."""
    # load reference data
    path = cached_schema_path('schema_org_shacl')
    if format == 'dict':
        dat = json.loads(path.read_text())
    else:
        dat = Graph().parse(path).serialize(format=format)

    g = Graph()

    if format != 'dict':
        benchmark(g.parse, data=dat, format=format)

    else:

        def parse_from_dict(dct):
            return g.parse(data=json.dumps(dct), format='json-ld')

        benchmark(parse_from_dict, dat)


# Oxigraph surprisingly is about the same speed wrt. parsing/dumping
# Also, it is sadly still buggy
@pytest.mark.skip()
def test_benchmark_parse_graph(rdf_stores, benchmark):
    """Compare parsing speed for different rdflib backends."""

    def parse_graph(path):
        return Graph().parse(path)

    benchmark(parse_graph, cached_schema_path('schema_org_shacl'))


test_data = [
    {
        '@context': 'https://schema.org/',
        '@id': 'https://doi.org/10.14278/rodare.373',
        '@type': 'Dataset',
        'creator': [
            {'@id': 'https://orcid.org/0000-0002-8001-3404', '@type': 'Person', 'name': 'Assis Dias, Felipe de'},
            {
                'affiliation': {
                    '@type': 'Organization',
                    'name': 'Multiphase Flow Research Center, Universidade Tecnol\u00f3gica Federal do Paran\u00e1, Brazil',
                },
                'name': 'Nunes Dos Santos, Eduardo',
            },
            {
                'affiliation': {
                    '@type': 'Organization',
                    'name': 'Multiphase Flow Research Center, Universidade Tecnol\u00f3gica Federal do Paran\u00e1, Brazil',
                },
                'name': 'Da Silva, Marco Jose',
            },
            {'@type': 'Person', 'name': 'Schleicher, Eckhard'},
            {
                '@type': 'Person',
                'affiliation': {
                    '@type': 'Organization',
                    'name': 'Multiphase Flow Research Center, Universidade Tecnol\u00f3gica Federal do Paran\u00e1, Brazil',
                },
                'name': 'Morales, R. E. M.',
            },
            {
                '@type': 'Person',
                'affiliation': {'@type': 'Organization', 'name': 'Faculty of Engineering, University of Nottingham, United Kingdom'},
                'name': 'Hewakandamby, B.',
            },
            {'@id': 'https://orcid.org/0000-0002-7371-0148', '@type': 'Person', 'name': 'Hampel, Uwe'},
        ],
        'datePublished': '2020-06-16',
        'description': '<p>Data set used on the work &quot;New Algorithm to Discriminate Phase Distribution of Gas-Oil-Water Pipe Flow with Dual-Modality Wire-Mesh Sensor&quot;.</p>\n\n<p>Data were acquired using a dual-modality wire-mesh sensor designed by the Brazilian partner UTFPR. The experiments were performed at the University of Nottingham in a water-oil liquid-liquid flow loop.</p>\n\n<p>However, the gas phase was introduced into the system to perform stratified three-phase flow measurements as a proof of concept. In this set of data, you find the calibrated amplitude and phase signals of nine points as well as permittivity and conductivity estimations (post-processing).</p>',
        'identifier': 'https://doi.org/10.14278/rodare.373',
        'inLanguage': {'@type': 'Language', 'alternateName': 'eng', 'name': 'English'},
        'keywords': ['complex impedance, flow visualization, gas-oil-water horizontal flow, three-phase, wire-mesh sensor'],
        'name': 'New algorithm to discriminate phase distribution of gas-oil-water pipe flow with dual-modality wire-mesh sensor - Data set',
        'sameAs': ['https://www.hzdr.de/publications/Publ-31152'],
        'url': 'https://rodare.hzdr.de/record/373',
    }
]


# shacl_validate of schema.org shapes of the test record takes ~430ms, regardless of the backend.
# This is pretty bad, because we cannot make pyshacl faster -> SHACL validation is very expensive!
# -> Another argument to pragmatically use pydantic data models and validating in Python-land.
@pytest.mark.skip()
def test_pyshacl(rdf_stores, benchmark):
    """Test speed of pyshacl validation."""
    from pyshacl import validate as shacl_validate
    from data_harvesting.data_model import SCHEMA_ORG_SHAPE

    g = Graph().parse(data=json.dumps(test_data), format='json-ld')
    benchmark(shacl_validate, g, shacl_graph=SCHEMA_ORG_SHAPE)


# validate_rdf 2x parses a JSON-LD dict to a Graph and 2x calls shacl_validate, this adds up to over ~1.3s !
@pytest.mark.skip()
def test_rdf_validation_ld(benchmark):
    """Test speed of validate_rdf function."""
    obj = LinkedDataObject(original=test_data)
    assert benchmark(obj.validate_rdf)


test_data2 = {
    'metadata': {
        'created_at': '2023-08-16T13:51:14.093689',
        'last_modified_at': '2023-08-16T13:51:14.093689',
        'uuid': '18bedbf2-8a2b-4365-be34-dc94135636a3',
    },
    'original': {
        '@context': 'http://schema.org',
        '@id': 'https://doi.org/10.26165/juelich-data/bmnagt',
        '@type': 'Dataset',
        'author': {
            '@id': 'https://orcid.org/0000-0003-0008-514X',
            '@type': 'Person',
            'affiliation': {'@type': 'Organization', 'name': '(Forschungszentrum J\u00fclich GmbH, Peter Gr\u00fcnberg Institute PGI-7)'},
            'familyName': 'B\u00e4umer',
            'givenName': 'Christoph',
            'name': 'Christoph B\u00e4umer',
        },
        'datePublished': '2020',
        'description': 'Measurements are provided in the folder either as Image files, raw data, or tabulated data. The original data and corresponding images for scanning measurements like AFM are deposited in folders named after the figure in the publication. The DFT data is available at Catalysis-hub.org under https://www.catalysis-hub.org/publications/BaeumerTuning2020. All other files were converted to ASCII and the plots were produced using OriginPro. The OriginPro file including all such data is also accessible and ordered corresponding to the figure labels in the publication.',
        'editor': {
            '@type': 'Person',
            'affiliation': {'@type': 'Organization', 'name': '(Forschungszentrum J\u00fclich GmbH, Peter Gr\u00fcnberg Institute PGI-7)'},
            'contributorType': 'ContactPerson',
            'familyName': 'B\u00e4umer',
            'givenName': 'Christoph',
            'name': 'Christoph B\u00e4umer',
        },
        'name': 'Replication Data for: "Tuning electrochemically-driven surface transformation in atomically-flat LaNiO3 thin films for enhanced water electrolysis"',
        'provider': {'@type': 'Organization', 'name': 'datacite'},
        'publisher': {'@type': 'Organization', 'name': 'J\u00fclich DATA'},
        'schemaVersion': 'http://datacite.org/schema/kernel-4',
        'url': 'https://data.fz-juelich.de/citation?persistentId=doi:10.26165/JUELICH-DATA/BMNAGT',
    },
    'derived': {
        '@context': 'http://schema.org',
        '@id': 'https://doi.org/10.26165/juelich-data/bmnagt',
        '@type': 'Dataset',
        'author': {
            '@id': 'https://orcid.org/0000-0003-0008-514X',
            '@type': 'Person',
            'affiliation': {'@type': 'Organization', 'name': '(Forschungszentrum J\u00fclich GmbH, Peter Gr\u00fcnberg Institute PGI-7)'},
            'familyName': 'B\u00e4umer',
            'givenName': 'Christoph',
            'name': 'Christoph B\u00e4umer',
        },
        'datePublished': '2020',
        'description': 'Measurements are provided in the folder either as Image files, raw data, or tabulated data. The original data and corresponding images for scanning measurements like AFM are deposited in folders named after the figure in the publication. The DFT data is available at Catalysis-hub.org under https://www.catalysis-hub.org/publications/BaeumerTuning2020. All other files were converted to ASCII and the plots were produced using OriginPro. The OriginPro file including all such data is also accessible and ordered corresponding to the figure labels in the publication.',
        'editor': {
            '@type': 'Person',
            'affiliation': {'@type': 'Organization', 'name': '(Forschungszentrum J\u00fclich GmbH, Peter Gr\u00fcnberg Institute PGI-7)'},
            'contributorType': 'ContactPerson',
            'familyName': 'B\u00e4umer',
            'givenName': 'Christoph',
            'name': 'Christoph B\u00e4umer',
        },
        'name': 'Replication Data for: "Tuning electrochemically-driven surface transformation in atomically-flat LaNiO3 thin films for enhanced water electrolysis"',
        'provider': {'@type': 'Organization', 'name': 'datacite'},
        'publisher': {'@type': 'Organization', 'name': 'J\u00fclich DATA'},
        'schemaVersion': 'http://datacite.org/schema/kernel-4',
        'url': 'https://data.fz-juelich.de/citation?persistentId=doi:10.26165/JUELICH-DATA/BMNAGT',
    },
    'patch_stack': [],
}


# Aggregator is slow mainly due to rdflib loading schema.org from the internet -> need caching!
# without cache: ~700ms, with cache: ~70ms
@pytest.mark.skip()
@pytest.mark.parametrize('use_cache', (False, True))
def test_agg_caching_speedup(use_cache, tmp_path, benchmark):
    import vcr

    def run():
        obj = LinkedDataObject.from_dict(test_data2)
        test_config = TEST_DATA_DIR / 'util' / 'agg_src' / 'config.yaml'
        agg = Aggregator(config_path=test_config)
        benchmark(agg.apply_to, obj)

    record_mode = 'none' if use_cache else 'all'
    with vcr.use_cassette(tmp_path.parent / 'aggregator.yaml', record_mode=record_mode, allow_playback_repeats=True):
        run()
