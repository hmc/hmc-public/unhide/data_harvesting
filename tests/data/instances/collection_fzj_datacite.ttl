@prefix schema: <http://schema.org/> .

<https://doi.org/10.1186/s40168-022-01401-0> a schema:ScholarlyArticle ;
    schema:isBasedOn <https://doi.org/10.6084/m9.figshare.c.6319689> .

<https://doi.org/10.6084/m9.figshare.c.6319689> a schema:Collection ;
    schema:additionalType <file:///home/j.broeder/work/git/data_harvesting/tests/data/instances/Collection> ;
    schema:author [ schema:affiliation <https://ror.org/02hpadn98> ;
            schema:familyName "Maraci" ;
            schema:givenName "Öncü" ;
            schema:name "Öncü Maraci" ],
        [ schema:affiliation <https://ror.org/02hpadn98> ;
            schema:familyName "Caspers" ;
            schema:givenName "Barbara A." ;
            schema:name "Barbara A. Caspers" ],
        [ schema:affiliation <https://ror.org/02hpadn98>,
                <https://ror.org/046ak2485> ;
            schema:familyName "Antonatou-Papaioannou" ;
            schema:givenName "Anna" ;
            schema:name "Anna Antonatou-Papaioannou" ],
        [ schema:affiliation <https://ror.org/02hpadn98> ;
            schema:familyName "Castillo-Gutiérrez" ;
            schema:givenName "Omar" ;
            schema:name "Omar Castillo-Gutiérrez" ],
        [ schema:affiliation <https://ror.org/02hpadn98>,
                <https://ror.org/02nv7yv05> ;
            schema:familyName "Jünemann" ;
            schema:givenName "Sebastian" ;
            schema:name "Sebastian Jünemann" ],
        [ schema:affiliation <https://ror.org/02hpadn98> ;
            schema:familyName "Engel" ;
            schema:givenName "Kathrin" ;
            schema:name "Kathrin Engel" ],
        [ schema:affiliation <https://ror.org/02hpadn98> ;
            schema:familyName "Kalinowski" ;
            schema:givenName "Jörn" ;
            schema:name "Jörn Kalinowski" ],
        [ schema:affiliation <https://ror.org/02hpadn98> ;
            schema:familyName "Busche" ;
            schema:givenName "Tobias" ;
            schema:name "Tobias Busche" ] ;
    schema:dateCreated "2022-11-28"^^schema:Date ;
    schema:dateModified "2022-11-28"^^schema:Date ;
    schema:datePublished "2022"^^schema:Date ;
    schema:description "Abstract Background The establishment of the gut microbiota in early life is a critical process that influences the development and fitness of vertebrates. However, the relative influence of transmission from the early social environment and host selection throughout host ontogeny remains understudied, particularly in avian species. We conducted conspecific and heterospecific cross-fostering experiments in zebra finches (Taeniopygia guttata) and Bengalese finches (Lonchura striata domestica) under controlled conditions and repeatedly sampled the faecal microbiota of these birds over the first 3 months of life. We thus documented the development of the gut microbiota and characterised the relative impacts of the early social environment and host selection due to species-specific characteristics and individual genetic backgrounds across ontogeny by using 16S ribosomal RNA gene sequencing. Results The taxonomic composition and community structure of the gut microbiota changed across ontogenetic stages; juvenile zebra finches exhibited higher alpha diversity than adults at the post-breeding stage. Furthermore, in early development, the microbial communities of juveniles raised by conspecific and heterospecific foster parents resembled those of their foster family, emphasising the importance of the social environment. In later stages, the social environment continued to influence the gut microbiota, but host selection increased in importance. Conclusions We provided a baseline description of the developmental succession of gut microbiota in zebra finches and Bengalese finches, which is a necessary first step for understanding the impact of the early gut microbiota on host fitness. Furthermore, for the first time in avian species, we showed that the relative strengths of the two forces that shape the establishment and maintenance of the gut microbiota (i.e. host selection and dispersal from the social environment) change during development, with host selection increasing in importance. This finding should be considered when experimentally manipulating the early-life gut microbiota. Our findings also provide new insights into the mechanisms of host selection. Video Abstract" ;
    schema:keywords "Genetics, FOS: Biological sciences" ;
    schema:license <https://creativecommons.org/licenses/by/4.0/legalcode> ;
    schema:name "Timing matters: age-dependent impacts of the social environment and host selection on the avian gut microbiota" ;
    schema:provider [ a schema:Organization ;
            schema:name "datacite" ] ;
    schema:publisher [ a schema:Organization ;
            schema:name "figshare" ] ;
    schema:schemaVersion "http://datacite.org/schema/kernel-4" ;
    schema:url <https://springernature.figshare.com/collections/Timing_matters_age-dependent_impacts_of_the_social_environment_and_host_selection_on_the_avian_gut_microbiota/6319689> .

<https://ror.org/02nv7yv05> a schema:Organization ;
    schema:name "Forschungszentrum Jülich" .

<https://ror.org/046ak2485> a schema:Organization ;
    schema:name "Freie Universität Berlin" .

<https://ror.org/02hpadn98> a schema:Organization ;
    schema:name "Bielefeld University" .
