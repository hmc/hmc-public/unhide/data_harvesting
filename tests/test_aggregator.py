# -*- coding: utf-8 -*-
"""
Tests for the data_model module which contains the Aggregator class and the LinkedDataObject class.
"""
from pprint import pprint

import rdflib

from data_harvesting.aggregator import Aggregator
from data_harvesting.data_model import LinkedDataObject
from data_harvesting.rdfpatch import RDFPatch

from helpers import TEST_DATA_DIR

test_data = [
    {
        '@context': 'https://schema.org/',
        '@id': 'https://doi.org/10.14278/rodare.373',
        '@type': 'Dataset',
        'creator': [
            {'@id': 'https://orcid.org/0000-0002-8001-3404', 'name': 'Assis Dias, Felipe de'},
            {
                'affiliation': {'name': 'Multiphase Flow Research Center, Universidade Tecnol\u00f3gica Federal do Paran\u00e1, Brazil'},
                'name': 'Nunes Dos Santos, Eduardo',
            },
            {
                'affiliation': {'name': 'Multiphase Flow Research Center, Universidade Tecnol\u00f3gica Federal do Paran\u00e1, Brazil'},
                'name': 'Da Silva, Marco Jose',
            },
            {'@type': 'Person', 'name': 'Schleicher, Eckhard'},
            {
                '@type': 'Person',
                'affiliation': {
                    '@type': 'Organization',
                    'name': 'Multiphase Flow Research Center, Universidade Tecnol\u00f3gica Federal do Paran\u00e1, Brazil',
                },
                'name': 'Morales, R. E. M.',
            },
            {
                '@type': 'Person',
                'affiliation': {'@type': 'Organization', 'name': 'Faculty of Engineering, University of Nottingham, United Kingdom'},
                'name': 'Hewakandamby, B.',
            },
            {'@id': 'https://orcid.org/0000-0002-7371-0148', '@type': 'Person', 'name': 'Hampel, Uwe'},
        ],
        'datePublished': '2020-06-16',
        'description': '<p>Data set used on the work &quot;New Algorithm to Discriminate Phase Distribution of Gas-Oil-Water Pipe Flow with Dual-Modality Wire-Mesh Sensor&quot;.</p>\n\n<p>Data were acquired using a dual-modality wire-mesh sensor designed by the Brazilian partner UTFPR. The experiments were performed at the University of Nottingham in a water-oil liquid-liquid flow loop.</p>\n\n<p>However, the gas phase was introduced into the system to perform stratified three-phase flow measurements as a proof of concept. In this set of data, you find the calibrated amplitude and phase signals of nine points as well as permittivity and conductivity estimations (post-processing).</p>',
        'identifier': 'https://doi.org/10.14278/rodare.373',
        'inLanguage': {'@type': 'Language', 'alternateName': 'eng', 'name': 'English'},
        'keywords': ['complex impedance, flow visualization, gas-oil-water horizontal flow, three-phase, wire-mesh sensor'],
        'name': 'New algorithm to discriminate phase distribution of gas-oil-water pipe flow with dual-modality wire-mesh sensor - Data set',
        'sameAs': ['https://www.hzdr.de/publications/Publ-31152'],
        'url': 'https://rodare.hzdr.de/record/373',
    }
]


def test_aggregator():
    """
    Test if an aggregator class can be properly initialized and applied to a LinkedData object
    """

    agg = Aggregator(config_path=TEST_DATA_DIR / 'util' / 'agg_src' / 'config.yaml')

    # should only contain stack parsed from config
    assert agg.stack != []

    # print(ag.stack_config)
    obj = LinkedDataObject(original=test_data)

    # obj_org = obj.copy()

    # TODO better use a controlled uplifting tasks stack instead of config stack:
    pprint(obj.derived)
    print(obj.patch_stack)
    # obj2 = benchmark(agg.apply_to, data_object=obj)
    obj2 = agg.apply_to(data_object=obj)
    print(obj2.patch_stack)
    # pprint(obj2.derived)
    assert isinstance(obj2, LinkedDataObject)

    # TODO real tests...
    pprint(obj2.model_dump())

    for patch in obj2.patch_stack:
        assert isinstance(patch, RDFPatch)
        assert isinstance(patch.add_triples, rdflib.Graph)
        # each operation has added at least some triples, currently not the case...
        # assert len(patch.add_triples) > 0
