# -*- coding: utf-8 -*-
"""
Tests for the data_model module which contains the Aggregator class and the LinkedDataObject class.
"""

from data_harvesting.data_model import LinkedDataObject
from data_harvesting.rdfpatch import RDFPatch
# import pytest

test_data = [
    {
        '@context': 'https://schema.org/',
        '@id': 'https://doi.org/10.14278/rodare.373',
        '@type': 'Dataset',
        'creator': [
            {'@id': 'https://orcid.org/0000-0002-8001-3404', '@type': 'Person', 'name': 'Assis Dias, Felipe de'},
            {
                'affiliation': {
                    '@type': 'Organization',
                    'name': 'Multiphase Flow Research Center, Universidade Tecnol\u00f3gica Federal do Paran\u00e1, Brazil',
                },
                'name': 'Nunes Dos Santos, Eduardo',
            },
            {
                'affiliation': {
                    '@type': 'Organization',
                    'name': 'Multiphase Flow Research Center, Universidade Tecnol\u00f3gica Federal do Paran\u00e1, Brazil',
                },
                'name': 'Da Silva, Marco Jose',
            },
            {'@type': 'Person', 'name': 'Schleicher, Eckhard'},
            {
                '@type': 'Person',
                'affiliation': {
                    '@type': 'Organization',
                    'name': 'Multiphase Flow Research Center, Universidade Tecnol\u00f3gica Federal do Paran\u00e1, Brazil',
                },
                'name': 'Morales, R. E. M.',
            },
            {
                '@type': 'Person',
                'affiliation': {'@type': 'Organization', 'name': 'Faculty of Engineering, University of Nottingham, United Kingdom'},
                'name': 'Hewakandamby, B.',
            },
            {'@id': 'https://orcid.org/0000-0002-7371-0148', '@type': 'Person', 'name': 'Hampel, Uwe'},
        ],
        'datePublished': '2020-06-16',
        'description': '<p>Data set used on the work &quot;New Algorithm to Discriminate Phase Distribution of Gas-Oil-Water Pipe Flow with Dual-Modality Wire-Mesh Sensor&quot;.</p>\n\n<p>Data were acquired using a dual-modality wire-mesh sensor designed by the Brazilian partner UTFPR. The experiments were performed at the University of Nottingham in a water-oil liquid-liquid flow loop.</p>\n\n<p>However, the gas phase was introduced into the system to perform stratified three-phase flow measurements as a proof of concept. In this set of data, you find the calibrated amplitude and phase signals of nine points as well as permittivity and conductivity estimations (post-processing).</p>',
        'identifier': 'https://doi.org/10.14278/rodare.373',
        'inLanguage': {'@type': 'Language', 'alternateName': 'eng', 'name': 'English'},
        'keywords': ['complex impedance, flow visualization, gas-oil-water horizontal flow, three-phase, wire-mesh sensor'],
        'name': 'New algorithm to discriminate phase distribution of gas-oil-water pipe flow with dual-modality wire-mesh sensor - Data set',
        'sameAs': ['https://www.hzdr.de/publications/Publ-31152'],
        'url': 'https://rodare.hzdr.de/record/373',
    }
]

test_data2 = {
    'metadata': {'uuid': '123123'},
    'original': {'@context': 'http://xmlns.com/foaf/0.1/', '1': {'name': '2'}},
    'derived': {'@context': 'http://xmlns.com/foaf/0.1/', '1': {'name': '2'}, '3': {'name': '4'}},
    'patch_stack': [
        {
            'addprefix': '',
            'add_triples': '@prefix foaf: <http://xmlns.com/foaf/0.1/> .\n\n"3" foaf:name "4" .\n\n',
            'delete_triples': '\n',
            'metadata': {
                'function_module': 'data_harvesting.util.sparql_util',
                'function_name': 'apply_update',
                'creation_time': '2023-09-04T14:01:38.816800',
            },
        }
    ],
}
patch_data = RDFPatch(
    addprefix='',
    add_triples="""
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<3> foaf:name <4> .
""",
    delete_triples='\n',
    metadata={
        'function_module': 'data_harvesting.util.sparql_util',
        'function_name': 'apply_update',
        'creation_time': '2023-09-04T14:01:38.816800',
    },
)

test_data3 = {
    'metadata': {'uuid': '6753fbd6-503e-4cd3-b615-bf608e30b096'},
    'original': {'@context': 'http://xmlns.com/foaf/0.1/', '1': {'name': '2'}},
    'derived': {'@context': 'http://xmlns.com/foaf/0.1/', '1': {'name': '2'}, '3': {'name': '4'}},
    'patch_stack': [patch_data],
}

all_testdata = [test_data2, test_data3]

# @pytest.mark.paramtrize("data_e", all_testdata)
data_e = test_data3


def test_serialize_ldo_patch(tmp_path):  # data_e
    """
    Various test for a Linked data object:
    1. if it can be properly initialized if it generates some metadata
    2. test a round turn if the instance is serialized to a file and read back again if it contains
    the same

    This also tests a lot of rdfpatch, because we have to test a full serialization of the object with a patch_stack

    """
    # data_e = request.param
    # obj = LinkedDataObject.from_dict(test_data2)
    obj = LinkedDataObject(**data_e)

    assert isinstance(obj, LinkedDataObject)

    metadata = obj.metadata
    assert 'uuid' in metadata.model_dump().keys()

    original_data = obj.original
    assert original_data == data_e['original']

    patch_stack = obj.patch_stack
    assert len(patch_stack) == len(data_e['patch_stack'])
    assert isinstance(patch_stack[0], RDFPatch)

    destination = tmp_path / 'test.json'
    obj.serialize(destination)
    # obj.dump_model_json(
    assert destination.is_file()

    print(obj.model_dump())
    print(obj.model_dump_json())
    # destination.unlink()
    # testif serialization and read in result in the same content
    obj2 = LinkedDataObject.from_filename(destination)

    assert obj.metadata == obj2.metadata
    assert obj.original == obj2.original
    assert obj.derived == obj2.derived
    # assert obj.patch_stack == obj2.patch_stack # patch stacks contain different graph uuids
    # ...

    for i, patch in enumerate(obj.patch_stack):
        assert patch.model_dump() == obj2.patch_stack[i].model_dump()


def test_serialize_ldo_simple(tmp_path):
    """
    Various test for a Linked data object, as in simple, only this time with a given Patchstack:
    1. if it can be properly initialized if it generates some metadata
    2. test a round turn if the instance is serialized to a file and read back again if it contains
    the same

    """

    obj = LinkedDataObject(original=test_data)
    assert isinstance(obj, LinkedDataObject)

    metadata = obj.metadata
    assert 'uuid' in metadata.model_dump().keys()

    original_data = obj.original
    assert original_data == test_data

    patch_stack = obj.patch_stack
    assert not patch_stack

    destination = tmp_path / 'test.json'
    obj.serialize(destination)
    # obj.dump_model_json(
    assert destination.is_file()

    print(obj.model_dump())
    print(obj.model_dump_json())
    # destination.unlink()
    # testif serialization and read in result in the same content
    obj2 = LinkedDataObject.from_filename(destination)

    assert obj.metadata == obj2.metadata
    assert obj.original == obj2.original
    assert obj.derived == obj2.derived
    assert obj.patch_stack == obj2.patch_stack


def test_rdf_validation_ld():
    """
    Test if the shacl validation function of the  LinkedData object
    """

    obj = LinkedDataObject(original=test_data)
    assert isinstance(obj, LinkedDataObject)

    assert obj.validate_rdf()

    test_data_c = test_data.copy()
    first = test_data_c[0]
    del first['@type']
    test_data2[0] = first
    non_valid = LinkedDataObject(original=test_data_c)

    # it seems that removing the type of the object is fine
    assert non_valid.validate_rdf()

    test_data_c = test_data.copy()
    test_data_c[0]['creator'][4]['affiliation'] = test_data_c[0]['creator'][2]['affiliation']['name']

    # type person with an affiliation, which is a string is not valid
    non_valid = LinkedDataObject(original=test_data_c)

    assert not non_valid.validate_rdf()
