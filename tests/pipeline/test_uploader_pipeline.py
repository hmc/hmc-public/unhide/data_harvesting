# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
#################################################################################################
# This file is part of the data-harvesting package.                                             #
# For copyright and license information see the .reuse/dep5 file                                #
# The code is hosted at https://codebase.helmholtz.cloud/hmc/hmc-public/unhide/data_harvesting  #
# For further information please visit  https://www.helmholtz-metadaten.de/en                   #
#################################################################################################
from pathlib import Path

from helpers import filter_prefect_warnings


this_file = Path(__file__).resolve()
test_filepath = this_file.parent / 'test_file.json'
test_files = [test_filepath]

config = None


# test
@filter_prefect_warnings
def test_run_uploader(prefect_test_fixture, mock_sparql, monkeypatch):
    """Test the uploader flow of the data pipeline, this requires a mocked virtuoso database"""
    from data_harvesting.pipeline import run_uploader

    # prepare env
    monkeypatch.setenv('SPARQL_ENDPOINT', 'https://sparql.unhide.helmholtz-metadaten.de')
    monkeypatch.setenv('DEFAULT_GRAPH', 'http://purls.helmholtz-metadaten.de/helmholtzkg')

    # test
    run_uploader(test_files)  # fails silently
