# -*- coding: utf-8 -*-
#################################################################################################
# This file is part of the data-harvesting package.                                             #
# For copyright and license information see the .reuse/dep5 file                                #
# The code is hosted at https://codebase.helmholtz.cloud/hmc/hmc-public/unhide/data_harvesting  #
# For further information please visit  https://www.helmholtz-metadaten.de/en                   #
#################################################################################################
from pathlib import Path

from helpers import filter_prefect_warnings

this_file = Path(__file__).resolve()
test_filepath = this_file.parent / 'test_file.json'
test_files = [test_filepath]

config_indexer = None


@filter_prefect_warnings
def test_run_indexer(prefect_test_fixture, requests_mock):
    """Test the indexer flow of the data pipeline"""
    from data_harvesting.pipeline import run_indexer

    run_indexer(test_files, config=config_indexer)  # fails silently

    # check if 'Indexer finished!' was logged
    # How to do this

    # run_indexer(test_files, config=config_indexer, fail=True) # should fail loudly / No SolrDB
