# -*- coding: utf-8 -*-
#################################################################################################
# This file is part of the data-harvesting package.                                             #
# For copyright and license information see the .reuse/dep5 file                                #
# The code is hosted at https://codebase.helmholtz.cloud/hmc/hmc-public/unhide/data_harvesting  #
# For further information please visit  https://www.helmholtz-metadaten.de/en                   #
#################################################################################################
import yaml
import json
import shutil
from pathlib import Path

from helpers import filter_prefect_warnings


this_file = Path(__file__).resolve()
test_filepath = this_file.parent / 'test_file.json'

config_str = """
Aggregator:
  stack:
    - type: python
      method: data_harvesting.util.json_ld_util:add_missing_uris
      kwargs: {'path_prefix':'https://helmholtz-metadaten.de/.well-known/genid', 'alternative_ids': ['email']}
"""


config = yaml.safe_load(config_str)


@filter_prefect_warnings
def test_run_aggregator(prefect_test_fixture, tmpdir):
    """Test the aggreator flow of the data pipeline"""
    from data_harvesting.pipeline import run_aggregator

    conf_path = tmpdir / 'config.yaml'
    with open(conf_path, 'w') as fileo:
        yaml.dump(config, fileo)
    print(conf_path)

    temp_test_file = tmpdir / 'test_file.json'
    shutil.copyfile(test_filepath, temp_test_file)
    test_files = [Path(temp_test_file)]

    # test
    run_aggregator(test_files, config=Path(conf_path))

    # test that the file changed
    with open(temp_test_file, 'r') as fileo:
        data = str(json.load(fileo))
    with open(test_filepath, 'r') as fileo:
        data_org = str(json.load(fileo))

    assert data != data_org  # if this fails it means the aggregator did nothing.

    # test that .well-known/genid is in there
    assert '.well-known/genid' in data
    assert '.well-known/genid' not in data_org
