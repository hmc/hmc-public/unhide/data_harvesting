# -*- coding: utf-8 -*-
#################################################################################################
# This file is part of the data-harvesting package.                                             #
# For copyright and license information see the .reuse/dep5 file                                #
# The code is hosted at https://codebase.helmholtz.cloud/hmc/hmc-public/unhide/data_harvesting  #
# For further information please visit  https://www.helmholtz-metadaten.de/en                   #
#################################################################################################
import yaml
import shutil
from pathlib import Path

from helpers import filter_prefect_warnings

this_file = Path(__file__).resolve()
test_filepath = this_file.parent / 'test_file.json'

# regression test for full data pipeline

# config
config_str = """
DummyHarvester:
  sources:
    test:
        name: bla
        ror: blub
Aggregator:
  stack:
    - type: python
      method: data_harvesting.util.json_ld_util:add_missing_uris
      kwargs: {'path_prefix':'https://helmholtz-metadaten.de/.well-known/genid', 'alternative_ids': ['email']}
"""

config = yaml.safe_load(config_str)


# run tests
@filter_prefect_warnings
def test_pipeline_flow(tmpdir, prefect_test_fixture, monkeypatch, mock_sparql):
    """
    Test if the harvester prefect pipeline runs with a Dummy harvester
    """
    from data_harvesting.pipeline import run_pipeline

    conf_path = tmpdir / 'config.yaml'
    with open(conf_path, 'w') as fileo:
        yaml.dump(config, fileo)
    print(conf_path)

    monkeypatch.setenv('SPARQL_ENDPOINT', 'https://sparql.unhide.helmholtz-metadaten.de')
    monkeypatch.setenv('DEFAULT_GRAPH', 'http://purls.helmholtz-metadaten.de/helmholtzkg')

    # backup test_file
    dest = this_file.parent / 'test_file_back.json'
    shutil.copyfile(test_filepath, dest)

    # Tests
    run_pipeline(harvester='all', config=Path(conf_path), out=Path(tmpdir))

    # clean up last run file
    list_run_file = this_file.parent / 'DummyHarvester.all.last_run'
    list_run_file.unlink(missing_ok=True)
    list_run_file = this_file.parent / 'DummyHarvester.test.last_run'
    list_run_file.unlink(missing_ok=True)

    # clean up test_file
    shutil.copyfile(dest, test_filepath)
    dest.unlink(missing_ok=True)
