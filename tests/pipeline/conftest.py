# -*- coding: utf-8 -*-
#################################################################################################
# This file is part of the data-harvesting package.                                             #
# For copyright and license information see the .reuse/dep5 file                                #
# The code is hosted at https://codebase.helmholtz.cloud/hmc/hmc-public/unhide/data_harvesting  #
# For further information please visit  https://www.helmholtz-metadaten.de/en                   #
#################################################################################################
# Pipeline test specific fixtures

import pytest
from data_harvesting.harvester import BaseHarvester
from pathlib import Path
from typing import Optional
from data_harvesting.harvester import HARVESTER_CLASSMAP as harvester_classmap
from datetime import datetime
import shutil
import logging
import os


this_file = Path(__file__).resolve()


# consider to use this for the whole session, because it might be slow
@pytest.fixture(autouse=False, scope='session')
def prefect_test_fixture():
    """
    Fixture for a prefect environment to test flows, monkeypatching env variables is only possible in function scope.
    therefore we set and reset env variables...
    """
    import prefect.settings
    from prefect.testing.utilities import prefect_test_harness

    with prefect_test_harness():
        # enable log capture of harvester
        env_log = os.getenv('PREFECT_LOGGING_EXTRA_LOGGERS')
        os.environ['PREFECT_LOGGING_EXTRA_LOGGERS'] = 'data_harvesting,data-harvesting,DummyHarvester'
        prefect.settings.PREFECT_LOGGING_EXTRA_LOGGERS.value()
        yield
        # reset env after test
        if env_log is not None:
            os.environ['PREFECT_LOGGING_EXTRA_LOGGERS'] = env_log
        else:
            del os.environ['PREFECT_LOGGING_EXTRA_LOGGERS']


class DummyHarvester(BaseHarvester):
    """Dummy harvester for testing purposes, just returns links to local test data"""

    def run(
        self,
        source='all',
        since: Optional[datetime] = None,
        base_savepath: Optional[Path] = None,
        **kwargs,
    ):
        source = 'test'
        test_filepath = this_file.parent / 'test_file.json'
        # harvester_classmap = {'dummy': DummyHarvester}
        if base_savepath is not None:
            dest = base_savepath / 'test_file.json'
            base_savepath.mkdir(parents=True, exist_ok=True)
            shutil.copyfile(test_filepath, dest)
            test_filepath = dest

        links = [test_filepath]
        logging.info(f'DummyHarvester last run: {since}, {links}')
        print(f'print: DummyHarvester last run: {since}, {links}')
        self.append_last_harvest(links)
        self.set_last_run(source)
        self.dump_last_harvest()
        print(f'last_harvest: {self.last_harvest}')


# register dummy harvester
harvesters = list(harvester_classmap.keys())
harvester_classmap['dummy'] = DummyHarvester
# overwrite for all functionality
# harvester_classmap = {'dummy': DummyHarvester}
for key in harvesters:
    del harvester_classmap[key]


# the sparqlwrapper uses urllib2 directly and not requests, so we need to mock it and cannot just
# use requests_mock. We do this by doing as in their tests by mocking urlopener of Wrapper
# (https://github.com/RDFLib/sparqlwrapper/blob/master/test/test_wrapper.py)


class FakeResult(object):
    def __init__(self, request):
        self.request = request

    def getcode(self):
        return 200


def urlopener(request):
    return FakeResult(request)


@pytest.fixture
def mock_sparql():
    """make the urlopener of sparqlwrapper.wrapper to always return a fake response"""
    import SPARQLWrapper.Wrapper as _victim

    _victim.urlopener = urlopener
