# -*- coding: utf-8 -*-
#################################################################################################
# This file is part of the data-harvesting package.                                             #
# For copyright and license information see the .reuse/dep5 file                                #
# The code is hosted at https://codebase.helmholtz.cloud/hmc/hmc-public/unhide/data_harvesting  #
# For further information please visit  https://www.helmholtz-metadaten.de/en                   #
#################################################################################################
import yaml
from pathlib import Path

from helpers import filter_prefect_warnings

this_file = Path(__file__).resolve()

# config
config_str = """
DummyHarvester:
  sources:
    test:
        name: bla
        ror: blub
"""

config = yaml.safe_load(config_str)


# run tests
@filter_prefect_warnings
def test_harvester_flow(tmpdir, prefect_test_fixture):
    """
    Test if the harvester prefect pipeline runs with a Dummy harvester
    """
    from data_harvesting.pipeline import run_harvester

    conf_path = tmpdir / 'config.yaml'
    with open(conf_path, 'w') as fileo:
        yaml.dump(config, fileo)
    print(conf_path)
    res = run_harvester(harvester='dummy', config=Path(conf_path), out=Path(tmpdir))

    assert len(res) > 0
    assert 'test_file.json' in str(res[0])

    res_all = run_harvester(harvester='all', config=Path(conf_path), out=Path(tmpdir))
    # in this case this is a future, so if this will fail, something with that future went wrong
    assert len(res_all) > 0
    assert 'test_file.json' in str(res_all[0])

    # clean up last run file
    list_run_file = this_file.parent.parent / 'DummyHarvester.all.last_run'
    list_run_file.unlink(missing_ok=True)
    list_run_file = this_file.parent.parent / 'DummyHarvester.test.last_run'
    list_run_file.unlink(missing_ok=True)
