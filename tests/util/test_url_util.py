# -*- coding: utf-8 -*-

from data_harvesting.util.url_util import encode_url, conditional_encode_url


def test_encode_url():
    """Test if a url is encoded correctly"""
    url = 'https://rodare.hzdr.de/api/files/85881f15-ea40-4c60-875e-ed99ec97a6e4/221219_DP_1,2x10^E-5molL_pH11.txt'

    # https://rodare.hzdr.de/api/files/85881f15-ea40-4c60-875e-ed99ec97a6e4/221219_DP_1,2x10^E-5molL_pH11.txt"
    encoded_url = encode_url(url)
    should_url = 'https://rodare.hzdr.de/api/files/85881f15-ea40-4c60-875e-ed99ec97a6e4/221219_DP_1,2x10%5EE-5molL_pH11.txt'
    print(url)
    print(encoded_url)
    assert encoded_url == should_url


def test_conditional_encode_url():
    """Test the condition"""

    url = 'https://rodare.hzdr.de/api/files/85881f15-ea40-4c60-875e-ed99ec97a6e4/221219_DP_1,2x10^E-5molL_pH11.txt'
    encoded_url = conditional_encode_url(url)
    should_url = 'https://rodare.hzdr.de/api/files/85881f15-ea40-4c60-875e-ed99ec97a6e4/221219_DP_1,2x10%5EE-5molL_pH11.txt'
    print(encoded_url)
    print(url)
    assert encoded_url == should_url

    url2 = 'https://helmholtz-metadaten.de/en'
    encoded_url = conditional_encode_url(url2)
    print(encoded_url)
    assert encoded_url == url2
