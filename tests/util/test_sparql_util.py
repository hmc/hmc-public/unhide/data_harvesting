# -*- coding: utf-8 -*-
""" this test is used to test all sparql updates
which are in our case specific to schema.org
"""
from pathlib import Path

import pytest
from rdflib import Graph
from rdflib.compare import graph_diff
from rdflib.compare import isomorphic

from data_harvesting.util.sparql_util import apply_update
from data_harvesting.util.sparql_util import get_update_from_file

from helpers import TEST_DATA_DIR, PROJECT_ROOT_DIR

# TODO automatically collect these, for now hardcode the paths
this_file = Path(__file__).resolve()
data_dir = TEST_DATA_DIR / 'instances'
results_dir = data_dir / 'results'
rules_dir = PROJECT_ROOT_DIR / 'data_harvesting' / 'data_enrichment'


# Helper function to specify a group of test input files forming a test case
def sparql_test_case(file_rule: str, file_in: str, file_out: str):
    return (rules_dir / file_rule, data_dir / file_in, results_dir / file_out)


# Helper function to load a tuple with test case files correctly
def get_test_inputs(test_case_tuple):
    sparql_update = get_update_from_file(test_case_tuple[0])
    ingraph = Graph().parse(test_case_tuple[1])
    should_output = Graph().parse(test_case_tuple[2])
    return (sparql_update, ingraph, should_output)


# helper function for clear print if something fails
def dump_nt_sorted(graph):
    for lin in sorted(graph.serialize(format='nt').splitlines()):
        if lin:
            print(lin)


@pytest.mark.parametrize(
    'testcase',
    [
        sparql_test_case('schema_infer_person.rd', 'dataset_juelichdata.ttl', 'res_person_type.ttl'),
        sparql_test_case('schema_infer_org.rd', 'dataset_juelichdata.ttl', 'res_org_type.ttl'),
        # (# Currently I do not see the benefit of that rule?
        #    rules_dir / 'schema_set_org_iri.rd', data_dir / 'dataset_juelichdata.ttl',
        #    data_dir / 'results' / 'res_set_org_iri.ttl'
        # ),
    ],
)
def test_apply_patch_from_file(testcase):
    """
    params contains tuples with filepaths (sparql_update_file, test_data_file, test_output_file)
    """

    sparql_update, ingraph, should_output = get_test_inputs(testcase)

    patch, patched_graph = apply_update(ingraph, sparql_update=sparql_update)
    # print(patched_graph.serialize(format='ttl'))

    # Todo better and finer test.
    in_both, in_first, in_second = graph_diff(patched_graph, should_output)

    # inspect output if it fails
    # dump_nt_sorted(in_first)
    # print('#####')
    # dump_nt_sorted(in_second)

    assert isomorphic(patched_graph, should_output)
