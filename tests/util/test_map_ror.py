# -*- coding: utf-8 -*-
import json
from pprint import pprint
from rdflib import Graph
from data_harvesting.util.map_ror import map_ror_schema_org
from data_harvesting.util.json_ld_util import validate


def test_map_ror_schema_org():
    """
    Test if the ror mapping function returns valid schema.org data from a ror test data set
    """

    null = None
    test_data = {
        'id': 'https://ror.org/01sf06y89',
        'name': 'Macquarie University',
        'types': ['Education'],
        'links': ['http://mq.edu.au/'],
        'aliases': [],
        'acronyms': [],
        'status': 'active',
        'wikipedia_url': 'http://en.wikipedia.org/wiki/Macquarie_University',
        'labels': [],
        'email_address': null,
        'ip_addresses': [],
        'established': 1964,
        'country': {'country_code': 'AU', 'country_name': 'Australia'},
        'relationships': [
            {'type': 'Related', 'label': 'Sydney Hospital', 'id': 'https://ror.org/0402tt118'},
            {'type': 'Child', 'label': 'ARC Centre of Excellence for Core to Crust Fluid Systems', 'id': 'https://ror.org/03nk9pp38'},
            {'type': 'Child', 'label': 'ARC Centre of Excellence in Cognition and its Disorders', 'id': 'https://ror.org/044b7p696'},
            {'type': 'Child', 'label': 'ARC Centre of Excellence in Synthetic Biology', 'id': 'https://ror.org/01p2zg436'},
        ],
        'addresses': [
            {
                'line': null,
                'lat': -33.775259,
                'lng': 151.112915,
                'postcode': null,
                'primary': False,
                'city': 'Sydney',
                'state': 'New South Wales',
                'state_code': 'AU-NSW',
                'country_geonames_id': 2077456,
                'geonames_city': {
                    'id': 2147714,
                    'city': 'Sydney',
                    'nuts_level1': {'code': null, 'name': null},
                    'nuts_level2': {'code': null, 'name': null},
                    'nuts_level3': {'code': null, 'name': null},
                    'geonames_admin1': {'id': 2155400, 'name': 'New South Wales', 'ascii_name': 'New South Wales', 'code': 'AU.02'},
                    'geonames_admin2': {'id': null, 'name': null, 'ascii_name': null, 'code': null},
                    'license': {
                        'attribution': 'Data from geonames.org under a CC-BY 3.0 license',
                        'license': 'http://creativecommons.org/licenses/by/3.0/',
                    },
                },
            }
        ],
        'external_ids': {
            'ISNI': {'preferred': null, 'all': ['0000 0001 2158 5405']},
            'FundRef': {'preferred': null, 'all': ['501100001230']},
            'OrgRef': {'preferred': null, 'all': ['19735']},
            'Wikidata': {'preferred': null, 'all': ['Q741082']},
            'GRID': {'preferred': 'grid.1004.5', 'all': 'grid.1004.5'},
        },
    }

    schema_org = map_ror_schema_org(test_data)

    pprint(schema_org)
    assert isinstance(schema_org, dict)

    # this uses pyld and resolve urls
    # assert validate_jsonld_simple(schema_org)

    schema_org_graph = Graph()
    schema_org_graph.parse(data=json.dumps(schema_org), format='json-ld')
    assert validate(schema_org_graph, validate_against=None)  # None will use the default schema.org shacl shapes
