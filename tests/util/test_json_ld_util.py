# -*- coding: utf-8 -*-
#################################################################################################
# This file is part of the data-harvesting package.                                             #
# For copyright and license information see the .reuse/dep5 file                                #
# The code is hosted at https://codebase.helmholtz.cloud/hmc/hmc-public/unhide/data_harvesting  #
# For further information please visit  https://www.helmholtz-metadaten.de/en                   #
#################################################################################################
"""Module containing test for json-ld utility"""

from data_harvesting.util.json_ld_util import add_missing_types
from data_harvesting.util.json_ld_util import add_missing_uris
from data_harvesting.util.json_ld_util import gen_id


def test_gen_id():
    """
    Test if the generated Id/hash is always the same
    """
    test_data = {
        '@context': 'https://schema.org/',
        '@id': 'https://doi.org/10.14278/rodare.30',
        '@type': 'SoftwareSourceCode',
        'creator': [
            {
                '@id': 'https://orcid.org/0000-0003-1642-0459',
                '@type': 'Person',
                'affiliation': {
                    '@id': 'dummy://repo.com/ds/xyz/meta.json/creator_0/affiliation',
                    'name': 'Helmholtz-Zentrum Dresden-Rossendorf',
                },
                'name': 'Widera, René',
                'sameAs': ['dummy://repo.com/ds/xyz/meta.json/creator_0'],
            },
            {'@id': 'dummy://repo.com/ds/xyz/meta.json/creator_1', '@type': 'Person', 'name': 'Test Name'},
        ],
        'identifier': 'https://doi.org/10.14278/rodare.30',
        'sameAs': ['https://www.hzdr.de/publications/Publ-27585', 'dummy://repo.com/ds/xyz/meta.json'],
    }
    # test if two generations of the same data lead to the same hash
    id_1 = gen_id(test_data)
    id_2 = gen_id(test_data)
    # print(id_1)
    # print(id_2)
    assert id_1 == id_2

    # test if different data leads to different hash
    test_data_unsorted = {
        '@context': 'https://schema.org/',
        'identifier': 'https://doi.org/10.14278/rodare.30',
        'sameAs': ['https://www.hzdr.de/publications/Publ-27585', 'dummy://repo.com/ds/xyz/meta.json'],
        '@id': 'https://doi.org/10.14278/rodare.30',
        '@type': 'SoftwareSourceCode',
        'creator': [
            {
                'affiliation': {
                    '@id': 'dummy://repo.com/ds/xyz/meta.json/creator_0/affiliation',
                    'name': 'Helmholtz-Zentrum Dresden-Rossendorf',
                },
                '@id': 'https://orcid.org/0000-0003-1642-0459',
                '@type': 'Person',
                'name': 'Widera, René',
                'sameAs': ['dummy://repo.com/ds/xyz/meta.json/creator_0'],
            },
            {'@id': 'dummy://repo.com/ds/xyz/meta.json/creator_1', '@type': 'Person', 'name': 'Test Name'},
        ],
    }
    id_3 = gen_id(test_data_unsorted)
    # print(id_3)
    assert id_1 == id_3

    # test if order in dict, does not play a role

    test_data2 = {
        '@context': 'https://schema.org/',
        '@id': 'https://doi.org/10.14278/rodare.30',
        '@type': 'SoftwareSourceCode',
        'creator': [
            {
                '@id': 'https://orcid.org/0000-0003-1642-0459',
                '@type': 'Person',
                'affiliation': {
                    '@id': 'dummy://repo.com/ds/xyz/meta.json/creator_0/affiliation',
                    'name': 'Helmholtz-Zentrum Dresden-Rossendorf',
                },
                'name': 'Widera, René',
                'sameAs': ['dummy://repo.com/ds/xyz/meta.json/creator_0'],
            },
            {'@id': 'dummy://repo.com/ds/xyz/meta.json/creator_1', '@type': 'Person', 'name': 'Test Name'},
        ],
    }
    id_4 = gen_id(test_data2)
    # print(id_4)
    assert id_1 != id_4


def test_add_missing_uris():
    """
    Tests if uris are added to all entries, which needs it.
    This test is very basic currently
    """
    # TODO parameterize testing data
    start_data = {
        '@context': 'https://schema.org/',
        '@id': 'https://doi.org/10.14278/rodare.30',
        '@type': 'SoftwareSourceCode',
        'creator': [
            {
                '@id': 'https://orcid.org/0000-0003-1642-0459',
                '@type': 'Person',
                'affiliation': {'name': 'Helmholtz-Zentrum Dresden-Rossendorf'},
                'name': 'Widera, Ren\u00e9',
            },
            {'@type': 'Person', 'name': 'Test Name'},
        ],
        'identifier': 'https://doi.org/10.14278/rodare.30',
        'sameAs': ['https://www.hzdr.de/publications/Publ-27585'],
    }

    should_data = {
        '@context': 'https://schema.org/',
        '@id': 'https://doi.org/10.14278/rodare.30',
        '@type': 'SoftwareSourceCode',
        'creator': [
            {
                '@id': 'https://orcid.org/0000-0003-1642-0459',
                '@type': 'Person',
                'affiliation': {
                    '@id': 'dummy://repo.com/ds/xyz/meta.json/211647a3d32a4/creator_0/affiliation/4efe5d60ce34168e79ba1e326b29a0d5',
                    'name': 'Helmholtz-Zentrum Dresden-Rossendorf',
                },
                'name': 'Widera, René',
                'sameAs': ['dummy://repo.com/ds/xyz/meta.json/211647a3d32a4/creator_0/c347c7dbaadcce75688b25f3e14439ba'],
            },
            {
                '@id': 'dummy://repo.com/ds/xyz/meta.json/211647a3d32a4/creator_1/7725cab6eab7de3fabd590954b1bc7f2',
                '@type': 'Person',
                'name': 'Test Name',
            },
        ],
        'identifier': 'https://doi.org/10.14278/rodare.30',
        'sameAs': ['https://www.hzdr.de/publications/Publ-27585', 'dummy://repo.com/ds/xyz/meta.json/bd3362a12e1ea955a9a404d61e6e10ad'],
    }

    prefix = 'dummy://repo.com/ds/xyz/meta.json'
    res_data = add_missing_uris(start_data, prefix)

    # pprint(res_data)
    # for key, val in res_data.items()
    assert res_data == should_data

    # todo test if valid jsonpath


def test_add_missing_uris_list():
    """
    Tests if uris are added to all entries, which needs it.
    This test is very basic currently
    """
    start_data = [
        {
            '@context': 'https://schema.org/',
            '@id': 'https://doi.org/10.14278/rodare.30',
            '@type': 'SoftwareSourceCode',
            'creator': [
                {
                    '@id': 'https://orcid.org/0000-0003-1642-0459',
                    '@type': 'Person',
                    'affiliation': {'name': 'Helmholtz-Zentrum Dresden-Rossendorf'},
                    'name': 'Widera, Ren\u00e9',
                },
                {'@type': 'Person', 'name': 'Test Name'},
            ],
            'identifier': 'https://doi.org/10.14278/rodare.30',
            'sameAs': ['https://www.hzdr.de/publications/Publ-27585'],
            'spatialCoverage': {
                'geo': {
                    'address': 'Central Focus Area, Chile',
                    'polygon': [
                        [['-70.371', '-18.351']],
                        [['-69.48', '-17.534']],
                        [['-67.26', '-23.50']],
                        [['-69.21', '-27.33']],
                        [['-70.93', '-27.33']],
                        [['-70.371', '-18.351']],
                    ],
                }
            },
        },
        {
            '@id': 'https://orcid.org/0000-0003-1642-0459',
            '@type': 'Person',
            'affiliation': {'name': 'Helmholtz-Zentrum Dresden-Rossendorf'},
        },
    ]

    should_data = [
        {
            '@context': 'https://schema.org/',
            '@id': 'https://doi.org/10.14278/rodare.30',
            '@type': 'SoftwareSourceCode',
            'creator': [
                {
                    '@id': 'https://orcid.org/0000-0003-1642-0459',
                    '@type': 'Person',
                    'affiliation': {
                        '@id': 'dummy://repo.com/ds/xyz/meta.json/211647a3d32a4/creator_0/affiliation/4efe5d60ce34168e79ba1e326b29a0d5',
                        'name': 'Helmholtz-Zentrum Dresden-Rossendorf',
                    },
                    'name': 'Widera, René',
                    'sameAs': ['dummy://repo.com/ds/xyz/meta.json/211647a3d32a4/creator_0/c347c7dbaadcce75688b25f3e14439ba'],
                },
                {
                    '@id': 'dummy://repo.com/ds/xyz/meta.json/211647a3d32a4/creator_1/7725cab6eab7de3fabd590954b1bc7f2',
                    '@type': 'Person',
                    'name': 'Test Name',
                },
            ],
            'identifier': 'https://doi.org/10.14278/rodare.30',
            'sameAs': ['https://www.hzdr.de/publications/Publ-27585', 'dummy://repo.com/ds/xyz/meta.json/9c314ad7d76bde1d21c166bbaf1a571d'],
            'spatialCoverage': {
                '@id': 'dummy://repo.com/ds/xyz/meta.json/211647a3d32a4/spatialCoverage/7c06b7d199e4e569b2e3982d51250bbe',
                'geo': {
                    '@id': 'dummy://repo.com/ds/xyz/meta.json/211647a3d32a4/spatialCoverage/geo/7c9c6de619cacd3d32e4c59f1059f3a8',
                    'address': 'Central Focus Area, Chile',
                    'polygon': [
                        [['-70.371', '-18.351']],
                        [['-69.48', '-17.534']],
                        [['-67.26', '-23.50']],
                        [['-69.21', '-27.33']],
                        [['-70.93', '-27.33']],
                        [['-70.371', '-18.351']],
                    ],
                },
            },
        },
        {
            '@id': 'https://orcid.org/0000-0003-1642-0459',
            '@type': 'Person',
            'affiliation': {
                '@id': 'dummy://repo.com/ds/xyz/meta.json/211647a3d32a4/51fa4d47414df/affiliation/4efe5d60ce34168e79ba1e326b29a0d5',
                'name': 'Helmholtz-Zentrum Dresden-Rossendorf',
            },
            'sameAs': ['dummy://repo.com/ds/xyz/meta.json/18e497fd4201fccc38fe8545ebff3e8e'],
        },
    ]

    prefix = 'dummy://repo.com/ds/xyz/meta.json'
    res_data = add_missing_uris(start_data, prefix)

    # pprint(res_data)
    # for key, val in res_data.items()
    assert res_data == should_data


def test_missing_uris_event():
    """
    There was a bug that this returned an empty graph in some cases
    """
    start_data = {
        '@context': 'http://schema.org',
        '@id': 'https://events.hifis.net/event/1271/',
        '@type': 'Event',
        'description': 'After the successful lunch-to-lunch meeting in Heidelberg (22.11-23.11.2023), we are happy to invite you to the lunch-to-lunch meeting in Braunschweig (13-14.05.2024).\u00a0The goal is to enable to meet in-person and to discuss cross-topic issues. The agenda is intended as a guide and may be amended to best serve the purpose of the lunch-to-lunch meeting.\u00a0Please register here: https://events.hifis.net/event/1271/manage/registration/1341/registrations/We are looking forward to seeing you there.\u00a0Mattea, Stephanie and Cordula\u00a0',
        'endDate': '2024-05-14T13:25:00+02:00',
        'location': {
            '@type': 'Place',
            'address': 'Helmholtz Centre for Infection Research, Inhoffenstra\u00dfe 7, 38124 Braunschweig',
            'name': 'Braunschweig, HZI (Science Campus, St\u00f6ckheim)',
        },
        'name': 'NFDI4Microbiota Lunch-to-Lunch Meeting',
        'startDate': '2024-05-13T11:30:00+02:00',
        'url': 'https://events.hifis.net/event/1271/',
    }

    should_data = {
        '@context': 'http://schema.org',
        '@id': 'https://events.hifis.net/event/1271/',
        '@type': 'Event',
        'description': 'After the successful lunch-to-lunch meeting in Heidelberg '
        '(22.11-23.11.2023), we are happy to invite you to the '
        'lunch-to-lunch meeting in Braunschweig (13-14.05.2024).\xa0'
        'The goal is to enable to meet in-person and to discuss '
        'cross-topic issues. The agenda is intended as a guide and may '
        'be amended to best serve the purpose of the lunch-to-lunch '
        'meeting.\xa0Please register here: '
        'https://events.hifis.net/event/1271/manage/registration/1341/registrations/We '
        'are looking forward to seeing you there.\xa0Mattea, Stephanie '
        'and Cordula\xa0',
        'endDate': '2024-05-14T13:25:00+02:00',
        'location': {
            '@id': 'dummy://repo.com/ds/xyz/meta.json/8e6d4275fe682/location/172c530bc269e24a76106b2b7e3751ff',
            '@type': 'Place',
            'address': 'Helmholtz Centre for Infection Research, ' 'Inhoffenstraße 7, 38124 Braunschweig',
            'name': 'Braunschweig, HZI (Science Campus, Stöckheim)',
        },
        'name': 'NFDI4Microbiota Lunch-to-Lunch Meeting',
        'sameAs': ['dummy://repo.com/ds/xyz/meta.json/2f012969706fb6660fc52865930e86cc'],
        'startDate': '2024-05-13T11:30:00+02:00',
        'url': 'https://events.hifis.net/event/1271/',
    }

    prefix = 'dummy://repo.com/ds/xyz/meta.json'
    res_data = add_missing_uris(start_data, prefix)

    # pprint(res_data)
    # for key, val in res_data.items()
    assert res_data == should_data


def test_add_missing_types():
    """
    Primitivly tests if a type is added
    """

    test_data = {
        '@context': 'https://schema.org/',
        '@id': 'https://doi.org/10.14278/rodare.30',
        '@type': 'SoftwareSourceCode',
        'creator': [
            {
                '@id': 'https://orcid.org/0000-0003-1642-0459',
                'affiliation': {'name': 'Helmholtz-Zentrum Dresden-Rossendorf'},
                'name': 'Widera, Ren\u00e9',
            },
            {'@type': 'Person', 'name': 'Test Name'},
        ],
        'identifier': 'https://doi.org/10.14278/rodare.30',
        'sameAs': ['https://www.hzdr.de/publications/Publ-27585'],
    }

    should_data = {
        '@context': 'https://schema.org/',
        '@id': 'https://doi.org/10.14278/rodare.30',
        '@type': 'SoftwareSourceCode',
        'creator': [
            {
                '@id': 'https://orcid.org/0000-0003-1642-0459',
                '@type': 'Person',
                'affiliation': {'name': 'Helmholtz-Zentrum Dresden-Rossendorf'},
                'name': 'Widera, Ren\u00e9',
            },
            {'@type': 'Person', 'name': 'Test Name'},
        ],
        'identifier': 'https://doi.org/10.14278/rodare.30',
        'sameAs': ['https://www.hzdr.de/publications/Publ-27585'],
    }

    res_data = add_missing_types(test_data)
    # pprint(res_data)
    assert res_data == should_data
