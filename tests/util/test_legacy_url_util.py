# -*- coding: utf-8 -*-
"""
Test for the url util function
"""

from data_harvesting.util.url_util import get_url_from_doi, get_domain_from_url, clean_pid


def test_get_url_from_doi():
    """Test function to resolve url from doi"""
    url_ref = 'https://odin.jrc.ec.europa.eu/alcor/DOIDisplay.html?p_RN5=200360144'
    url = get_url_from_doi('10.5290/200360144')
    assert url == url_ref

    # Nonsense  url
    url = get_url_from_doi('asdsagfefaef')
    assert url is None


def test_get_domain_from_url():
    """Test function to resolve url from doi"""
    assert get_domain_from_url('https://archive.materialscloud.org/record/2021.134') == 'archive.materialscloud.org'


def test_clean_pid():
    """Test function to get relevant part of pid"""
    assert clean_pid('http://dx.doi.org/10.5290/207200009') == '10.5290/207200009'
    assert clean_pid('https://doi.org/10.24435/materialscloud:nq-ht') == '10.24435/materialscloud:nq-ht'
