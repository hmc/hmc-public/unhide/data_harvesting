# -*- coding: utf-8 -*-
"""
Tests to thest the config file and related methods
"""
from data_harvesting import get_config


def test_get_config():
    """Test if config can be read and is therefore a valid yaml file"""

    config = get_config()

    assert isinstance(config, dict)
    assert len(list(config.keys())) > 0
