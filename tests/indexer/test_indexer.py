# -*- coding: utf-8 -*-
"""
here are tests for the indexer
"""
import json
from pathlib import Path

import pytest

from data_harvesting.indexer.indexer import Indexer

# collect test files
this_file = Path(__file__).resolve()
TEST_DATA_PATH = this_file.parent / 'test_data' / 'generic'

src_files = list(TEST_DATA_PATH.glob('./src/*.json'))
dest_files = [Path(str(entry).replace('/src/', '/dest/')) for entry in src_files]  # list(TEST_DATA_PATH.glob('dest/*.jsonld'))
# This could be fragile
# i.e if the same number is not given, better check for the hashes, same file name
# currently this will only work on linux.

files = [(entry, dest_files[i]) for i, entry in enumerate(src_files)]
inst_indexer = Indexer()


@pytest.mark.parametrize('testdatapath', files)
def test_indexer_run_file(testdatapath):
    """
    Regression test of the indexer for all files in test data

    Files need the same name in a src and a dest folder in the same folder
    """
    unhidedata = testdatapath[0].name.endswith('unhide.json')

    inst_indexer.index_file(testdatapath[0], fail=True, unhidedata=unhidedata)

    with open(testdatapath[1], 'r', encoding='utf-8') as fileo:
        should_data = json.load(fileo)

    assert inst_indexer.last_indexed_data == should_data
