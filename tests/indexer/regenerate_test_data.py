# -*- coding: utf-8 -*-
"""
script to generate the test data in test_data/dest from test_data/src
this is useful if there is a large change in the indexer, but be very careful if it works when you
update the test_data results
"""
# pylint: skip-file
import json
from pathlib import Path

from data_harvesting.indexer.indexer import Indexer

# collect test files
this_file = Path(__file__).resolve()
TEST_DATA_PATH = this_file.parent / 'test_data' / 'generic'

src_files = list(TEST_DATA_PATH.glob('./src/*.json'))
dest_files = [Path(str(entry).replace('/src/', '/dest/')) for entry in src_files]
files = [(entry, dest_files[i]) for i, entry in enumerate(src_files)]
in_indexer = Indexer()


def regenerate_test_data(testdatapath):
    """Regenerate test data for given input and store under given destination by running the indexer."""
    in_indexer.index_file(testdatapath[0], fail=True)
    result = in_indexer.last_indexed_data

    # write to file
    with open(testdatapath[1], 'w', encoding='utf-8') as fileo:
        json.dump(result, fileo)


for file_o in files:
    regenerate_test_data(file_o)
