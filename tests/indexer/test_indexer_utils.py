# -*- coding: utf-8 -*-
"""Contains tests for utlitly functions around the indexer"""
import pytest

from data_harvesting.indexer.indexer import extract_entity_dicts, generate_index_dict

TEST_JSONLD = {
    '@context': 'http://schema.org',
    '@id': 'https://doi.org/10.5880/gfz.1.4.2019.002',
    '@reverse': {'isBasedOn': {'@id': 'https://doi.org/10.26607/ijsl.v21i1.92', '@type': 'ScholarlyArticle'}},
    '@type': 'Dataset',
    'additionalType': 'Dataset',
    'author': [
        {
            '@type': 'Person',
            'affiliation': {'@type': 'Organization', 'name': 'Nazareth University, Rochester, New York, US'},
            'familyName': 'Hyde',
            'givenName': 'Emilee',
            'name': 'Emilee Hyde',
        },
        {
            '@id': 'https://orcid.org/0000-0001-7014-1843',
            '@type': 'Person',
            'affiliation': {'@type': 'Organization', 'name': 'GFZ German Research Centre for Geosciences, Potsdam, Germany'},
            'familyName': 'Kyba',
            'givenName': 'Christopher',
            'name': 'Christopher Kyba',
        },
    ],
    'citation': [
        {'@type': 'CreativeWork'},
        {'@id': 'https://doi.org/10.26607/ijsl.v19i2.79', '@type': 'CreativeWork'},
        {'@id': 'https://doi.org/10.3390/rs10121964', '@type': 'CreativeWork'},
    ],
    'contentSize': '2 Files',
    'dateCreated': '2019-02-25',
    'datePublished': '2019',
    'description': 'This dataset is related to the question of whether communities inside of certified "International Dark Sky Places" have different levels of lighting change in comparison to communities of similar size that are located further away. It is a supplement to Coesfeld et al. (2019), in which this question was examined for the time period 2012-2018. This dataset contains the boundaries of the analysis areas (i.e. community boundaries) in the directory "Coordinates and Graphs". These boundaries are stored as polygons in plain text format. Additional data related to the publication (e.g. Excel tables containing summary data of measured lighting trends for each community) are also included. Details of the data are available in the data description file.',
    'editor': [
        {
            '@type': 'Person',
            'affiliation': {'@type': 'Organization', 'name': 'Nazareth University, Rochester, New York, US'},
            'contributorType': 'Researcher',
            'familyName': 'Hyde',
            'givenName': 'Emilee',
            'name': 'Emilee Hyde',
        },
        {
            '@id': 'https://orcid.org/0000-0001-7014-1843',
            '@type': 'Person',
            'affiliation': {'@type': 'Organization', 'name': 'GFZ German Research Centre for Geosciences, Potsdam, Germany'},
            'contributorType': 'ProjectLeader',
            'familyName': 'Kyba',
            'givenName': 'Christopher',
            'name': 'Christopher Kyba',
        },
    ],
    'encodingFormat': ['application/octet-stream', 'application/octet-stream'],
    'funder': [
        {'@id': 'https://doi.org/10.13039/100010661', '@type': 'Organization', 'name': 'Horizon 2020 Framework Programme'},
        {'@id': 'https://doi.org/10.13039/501100001656', '@type': 'Organization', 'name': 'Helmholtz-Gemeinschaft'},
    ],
    'inLanguage': 'en',
    'keywords': 'International Dark Sky Places; International Dark Sky Park; International Dark Sky Reserve; Light pollution; Light emissions, Earth Observation Satellites &gt; Joint Polar Satellite System (JPSS) &gt; SUOMI-NPP, Earth Remote Sensing Instruments &gt; Passive Remote Sensing &gt; Spectrometers/Radiometers &gt; Imaging Spectrometers/Radiometers &gt; VIIRS',
    'license': 'http://creativecommons.org/licenses/by/4.0',
    'name': 'Analysis boundaries and lighting trends (2012-2018) for selected International Dark Sky Places',
    'provider': {'@type': 'Organization', 'name': 'datacite'},
    'publisher': {'@type': 'Organization', 'name': 'GFZ Data Services'},
    'spatialCoverage': {'@type': 'Place', 'geo': {'@type': 'GeoShape', 'box': '29.0247 -113.423 54.3468 12.9541'}},
    'url': 'http://dataservices.gfz-potsdam.de/panmetaworks/showshort.php?id=escidoc:3974890',
    'version': '1.0',
}

TEST_JSONLD = {
    '@type': 'Dataset',
    'name': 'dataset1',
    'author': [
        {'@type': 'Person', 'name': 'author1'},
        {'@type': 'Person', 'name': 'author2', 'affilliation': {'@type': 'Organization', 'name': 'org1'}},
    ],
}


def test_extract_entity_dicts():
    """
    Test functionality of extract entity dicts
    """
    should_res = [
        {
            '@type': 'Dataset',
            'name': 'dataset1',
            'author': [
                {'@type': 'Person', 'name': 'author1'},
                {'@type': 'Person', 'name': 'author2', 'affilliation': {'@type': 'Organization', 'name': 'org1'}},
            ],
        },
        {'@type': 'Person', 'name': 'author1'},
        {'@type': 'Person', 'name': 'author2', 'affilliation': {'@type': 'Organization', 'name': 'org1'}},
        {'@type': 'Organization', 'name': 'org1'},
    ]

    res = extract_entity_dicts(TEST_JSONLD)
    for ent in res:
        print('###')
        print(ent)
    assert len(res) == 4
    assert res == should_res


@pytest.mark.skip()
def test_generate_index_dict():
    """
    Test functionality of index generation, if the index for given data is corrected

    This test is very basic for now. should be improved
    """
    # output of extract_entities_dict
    data = [
        {
            '@type': 'Dataset',
            'name': 'dataset1',
            'author': [
                {'@type': 'Person', 'name': 'author1'},
                {'@type': 'Person', 'name': 'author2', 'affilliation': {'@type': 'Organization', 'name': 'org1'}},
            ],
        },
        {'@type': 'Person', 'name': 'author1'},
        {'@type': 'Person', 'name': 'author2', 'affilliation': {'@type': 'Organization', 'name': 'org1'}},
        {'@type': 'Organization', 'name': 'org1'},
    ]

    index_should = [{}, {}, {}]

    for i, data_e in enumerate(data):
        index = generate_index_dict(data_e)

        assert index_should[i] == index
