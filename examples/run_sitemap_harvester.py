# -*- coding: utf-8 -*-
from data_harvesting.harvester.sitemap import SitemapHarvester

url3 = 'https://juser.fz-juelich.de/sitemap-index.xml'
url2 = 'https://data.fz-juelich.de/sitemap.xml'
url = 'https://rodare.hzdr.de/sitemap.xml'
url4 = 'https://doi.pangaea.de/sitemap.xml'
url5 = 'https://massbank.eu/MassBank/sitemapindex.xml'
# sitemap = get_all_sitemaps(url)
# sitemap2 = get_all_sitemaps(url2)

# sitemap3 = get_all_sitemaps(url3)
# records = filter_urls(sitemap, match_pattern="*/record/\d")

# sitemap.to_csv('rodare_sitemap_df.csv')
# sitemap2.to_csv('juelichdata_sitemap_df.csv')
# sitemap3.to_csv('juser_sitemap_df.csv')
# print(sitemap)

# sh = SitemapHarvester(outpath='./juelichdata')
# sh.run(sitemap=url2, match_pattern='.*/dataset.xhtml.*', url_transforms=[{'replace' : ('dataset.xhtml?', 'api/datasets/export?exporter=schema.org&')}])

# sh = SitemapHarvester(outpath='./pangaea')
# sh.run(sitemap=url4)

sh = SitemapHarvester(outpath='./massbank')
sh.run(source=url5, match_pattern='.*/RecordDisplay.*')
