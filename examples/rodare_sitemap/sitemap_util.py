# -*- coding: utf-8 -*-

import advertools as adv
import extruct
import requests


def get_all_sitemaps(url):
    """From the top sitemap url get all sub urls and parse"""
    sitemap_df = adv.sitemap_to_df(url)
    return sitemap_df


def filter_urls(sitemap_df, since=None, match_pattern=None):
    """Return a list of urls from a given sitemap tree which have been updated since and which optional match a certain pattern"""
    sub_df = []

    return sub_df


def extract_metadata_url(url, syntaxes=['dublincore', 'jsonld', 'microdata', 'opengraph', 'rdfa']):
    """ """
    req = requests.get(url)
    base_url = get_base_url(r.text, r.url)
    data = extruct.extract(r.text, base_rul=base_url, syntaxes=syntaxes)

    return data


url = 'https://juser.fz-juelich.de/sitemap-index.xml'

sitemap = get_all_sitemaps(url)
records = filter_urls(sitemap, match_pattern='*/record/\d')

print(sitemap)
