# -*- coding: utf-8 -*-
import json
from pathlib import Path

import extruct
import requests
from bs4 import BeautifulSoup
from helper_functions import get_children_sitemaps
from helper_functions import is_sitemap_index
from helper_functions import url_filter

SITEMAP_URL: str = 'https://rodare.hzdr.de/sitemap.xml'
JSON_DIR: Path = Path(__file__).resolve().parent / 'json-ld_metadata'

# Navigate and parse the sitemap.

sitemap_response = requests.get(SITEMAP_URL)

xml_parser = BeautifulSoup(sitemap_response.content, 'lxml-xml')

if is_sitemap_index(xml_parser):
    sitemap_urls: list = get_children_sitemaps(xml_parser)

else:
    sitemap_urls: list = [SITEMAP_URL]

all_urls = []
for url in sitemap_urls:
    parser = BeautifulSoup(requests.get(url).content, 'lxml-xml')
    all_urls.extend(parser.find_all('url'))

# Remove all the xml syntax but the actual url.
raw_urls = [url.find('loc').text for url in all_urls]

filtered_urls = [url for url in raw_urls if url_filter(url)]

# Extract the JSON-LD using extruct.

JSON_DIR.mkdir(exist_ok=True)
for i, url in enumerate(filtered_urls):
    print(f'Fetching metadata from url {i+1}/{len(filtered_urls)}')

    response = requests.get(url)
    metadata = extruct.extract(response.text)

    filename = f"{url.replace('/', ':')}.json"
    json_filepath: Path = JSON_DIR / filename
    json_filepath.write_text(json.dumps(metadata))
