# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup


def is_sitemap_index(parser: BeautifulSoup) -> bool:
    """Check is a sitemap is of type index or of type urlset."""

    if parser.find_all('sitemapindex'):
        return True

    elif parser.find_all('urlset'):
        return False

    else:
        raise ValueError("The xml content doesn't correspond to a sitemap.")


def get_children_sitemaps(parent_sitemap: BeautifulSoup) -> list:
    """Get the children sitemap from an index sitemap"""

    children_blocks = parent_sitemap.find_all('sitemap')

    children_urls = []
    for block in children_blocks:
        children_urls.append(block.findNext('loc').text)

    return children_urls


def url_filter(url: str) -> bool:
    """Function to filter the urls that are used to fetch JSON-LD. If the url is
    valid, the function should return True, otherwise, it should return False.

    return True implies no filtering.
    """
    return True
