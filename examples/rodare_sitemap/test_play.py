# -*- coding: utf-8 -*-
import re

strings = [
    'http://juser.fz-juelich.de/record/877980/files',
    'http://juser.fz-juelich.de/record/877980',
    'http://juser.fz-juelich.de/record/877980/files/',
    'http://juser.fz-juelich.de/record/877980/',
]

pattern = None
antipattern = r'files'
for string in strings:
    if pattern is not None:
        if re.search(pattern, string):
            print(string)
    if antipattern is not None:
        if re.search(antipattern, string) is None:
            print(string)
