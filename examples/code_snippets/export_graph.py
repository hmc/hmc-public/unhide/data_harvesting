# -*- coding: utf-8 -*-
from rdflib import Graph

g = Graph()
g.parse('hgf_software_graph.json')
g.serialize(destination='hgf_software_graph.ttl')
