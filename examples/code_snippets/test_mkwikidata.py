# -*- coding: utf-8 -*-
import mkwikidata

query = """
SELECT DISTINCT ?cityLabel ?population ?gps
WHERE
{
  ?city wdt:P31/wdt:P279* wd:Q515 .
  ?city wdt:P1082 ?population .
  ?city wdt:P625 ?gps .
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en" .
  }
}
ORDER BY DESC(?population) LIMIT 100
"""

query = """
SELECT DISTINCT ?cityLabel ?programmingLanguage ?sourceCodeRepository
WHERE
{
  ?city wdt:P31/wdt:P279* wd:Q515 .
  ?
  ?
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "en" .
  }
}
LIMIT 10000
"""

query = """
SELECT DISTINCT ?item ?itemLabel WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
  {
    SELECT DISTINCT ?item WHERE {
      ?item p:P31 ?statement0.
      ?statement0 (ps:P31/(wdt:P279*)) wd:Q9143.
    }
    LIMIT 10000
  }
}
"""
# programming language (Q9143)
#  source code repository (P1324)
#  software version identifier (P348)
#   file extension (P1195)
#  official website (P856)
#  programming paradigm (P3966)
# user manual URL (P2078)
#  has use (P366)
#  MIME type (P1163)
#  programming paradigm (P3966)
query = """
SELECT DISTINCT ?item ?itemLabel ?itDescription ?WHERE {
  SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
  {
    SELECT DISTINCT ?item WHERE {
      ?item p:P31 ?statement0.
      ?statement0 (ps:P31/(wdt:P279*)) wd:Q9143.
    }
    LIMIT 10000
  }
}
"""
"""
SELECT DISTINCT ?item ?itemLabel ?itemDescription ?alias ?sourceCodeRepo ?fileExtension ?website ?mimetype ?hasuse ?usermanualURL
WHERE {
      ?item p:P31 ?statement0.
      ?statement0 (ps:P31/(wdt:P279*)) wd:Q9143.
  OPTIONAL{?item wdt:P856 ?website.}
  OPTIONAL{?item wdt:P1324 ?sourceCodeRepo.}
      OPTIONAL{?item wdt:P1195 ?fileExtension.}
      OPTIONAL{?item wdt:P1163 ?mimetype.}
      OPTIONAL{?item wdt:P366 ?hasuse.}
      OPTIONAL{?item wdt:P2078 ?usermanualURL.}
     SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
}
LIMIT 100
"""

query_result = mkwikidata.run_query(query, params={})
print(query_result)
data = [{'name': x['cityLabel']['value'], 'population': int(x['population']['value'])} for x in query_result['results']['bindings']]

import pandas as pd

pd.DataFrame(data).set_index('name').head(10).plot.barh().invert_yaxis()
