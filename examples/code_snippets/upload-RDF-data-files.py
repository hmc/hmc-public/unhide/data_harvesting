# -*- coding: utf-8 -*-
import rdflib
from SPARQLWrapper import DIGEST
from SPARQLWrapper import POST
from SPARQLWrapper import SPARQLWrapper


def insert_by_post(rdf, graph_name, endpoint_url, username, passwd):
    sparql = SPARQLWrapper(endpoint_url)
    sparql.setHTTPAuth(DIGEST)
    sparql.setCredentials(username, passwd)
    sparql.setMethod(POST)
    g = rdflib.Graph()
    g.namespace_manager.bind('schema', 'http://schema.org/')

    g.parse(data=rdf, format='ttl')
    # for ns_prefix, namespace in g.namespaces():
    #     print(namespace)
    triples = ''
    for s, p, o in g.triples((None, None, None)):
        triple = f'{s.n3()} {p.n3()} {o.n3()} . '
        triples += triple
    query = 'INSERT IN GRAPH <%s> { %s }' % (graph_name, triples)
    sparql.setQuery(query)
    results = sparql.query()
    if results.response.getcode() == 200:
        print('Your database was successfuly updated ... ()' + str(len(g)) + ') triple(s) have been added.')


if __name__ == '__main__':
    with open('test-data/dataset_juelichdata.ttl', 'r') as myfile:
        insert_by_post(myfile.read(), 'http://www.purl.com/test/my_graph', 'http://localhost:8890/sparql-auth', 'dba', 'dba')
