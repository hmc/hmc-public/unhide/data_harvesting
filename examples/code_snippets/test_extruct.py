# -*- coding: utf-8 -*-
import pprint

import extruct
import requests
from w3lib.html import get_base_url

url = 'https://juser.fz-juelich.de/record/910321'
url = 'https://www.wikidata.org/wiki/Q28865'

url = 'https://rodare.hzdr.de/record/1001'
url = 'https://zenodo.org/record/5972201#.Y24Oo0rMIUE'
url = 'https://dataservices.gfz-potsdam.de/panmetaworks/showshort.php?id=85b00ebc-8191-11eb-9603-497c92695674'
url = 'https://bib-pubdb1.desy.de/record/322'
url = 'https://elib.dlr.de/189186/'

url = 'https://www.nature.com/articles/d41586-022-03611-w'
url = 'https://doi.pangaea.de/10.1594/PANGAEA.950248'
url = 'https://data.fz-juelich.de/api/datasets/export?exporter=dataverse_json&persistentId=doi%3A10.26165/JUELICH-DATA/VGEHRD'

url = 'https://data.fz-juelich.de/api/datasets/export?exporter=schema.org&persistentId=doi%3A10.26165/JUELICH-DATA/VGEHRD'  # here is the nice json LD

url = 'https://doi.pangaea.de/10.1594/PANGAEA.54617'

url = 'https://repository.helmholtz-hzi.de/handle/10033/8409'
url = 'https://epic.awi.de/id/eprint/3/'
url = 'https://epic.awi.de/id/eprint/56478/'
url = 'https://www.frontiersin.org/articles/10.3389/fenvs.2022.889428/full'

url1 = 'https://geofon.gfz-potsdam.de/doi/network/1P/2014'
url1 = 'https://doi.pangaea.de/10.1594/PANGAEA.853987'
url = 'https://marine-data.de/?id=oai%3Apangaea.de%3Adoi%3A10.1594%2FPANGAEA.884659'
url = 'https://massbank.eu/MassBank/RecordDisplay?id=MSBNK-Keio_Univ-KO009192'
url = 'https://inspirehep.net/literature/2513441'
url = 'https://moses-data.gfz-potsdam.de/onestop/#/collections/details/AYDSHdC_ghVI18uYKsEx'
url = 'https://ror.org/02nv7yv05'
url = 'https://hcdc.hereon.de/geonetwork/srv/eng/catalog.search#/metadata/0cdab90e-53a2-470b-8523-e3c475debd27'

# sitemap gives this  https://data.fz-juelich.de/dataset.xhtml?persistentId=doi:10.26165/JUELICH-DATA/VGEHRD

pp = pprint.PrettyPrinter(indent=2)
r = requests.get(url)
base_url = get_base_url(r.text, r.url)
syntaxes = ['dublincore', 'json-ld', 'microdata', 'opengraph', 'rdfa']
data = extruct.extract(r.text, syntaxes=syntaxes, base_url=base_url)

pp.pprint(data)
# pp.pprint(r.json())
