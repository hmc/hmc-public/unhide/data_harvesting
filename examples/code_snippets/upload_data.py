# -*- coding: utf-8 -*-
"""
Upload all json/jsonld files files from a given directory into the
"""
from pathlib import Path

import progressbar
import requests

data_dir = './juelichdata/'  #'./rodare/' #'./pangaea'
data_dir = Path(data_dir).resolve()
database_name = 'playground2'  #'HMC_knowledge_graph_prototype'

# offset because fuseki crashed during upload, we assume that glob pics up the files in the same order...
offset = 0  # 180000+108162
files = list(data_dir.glob('**/*.json*'))[offset:]
format_upload = 'text/turtle'  #'application/ld+json'#'text/turtle'
files = [
    './juelichdata_ttl/68747470733a2f2f646174612e667a2d6a75656c6963682e64652f6170692f64617461736574732f6578706f72743f6578706f727465723d736368656d612e6f72672670657273697374656e7449643d646f693a31302e32363136352f4a55454c4943482d444154412f5a4541345356.ttl'
]
files1 = [
    './juelichdata/68747470733a2f2f646174612e667a2d6a75656c6963682e64652f6170692f64617461736574732f6578706f72743f6578706f727465723d736368656d612e6f72672670657273697374656e7449643d646f693a31302e32363136352f4a55454c4943482d444154412f5a4541345356.json'
]

nfiles = len(files)
print(f'Uploading files: {nfiles}')
with progressbar.ProgressBar(max_value=nfiles) as bar:
    for i, fileo in enumerate(files):
        bar.update(i)
        data = open(fileo, 'r').read()
        data = data.encode()  # will prooduce bytes object encoded with utf-8
        headers = {'Content-Type': f'{format_upload};charset=utf-8'}
        req = requests.post(f'http://localhost:3030/{database_name}/data?default', data=data, headers=headers)
