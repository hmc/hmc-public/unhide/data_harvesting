# -*- coding: utf-8 -*-
import json
from urllib.parse import quote

import pandas as pd
import progressbar
import requests

# csv of the form
# number, id, name, type, json_content, ror_api_match, ror_api_score, ror_api_match_type, ror_api_output_json

files = [f'{i}_pageapi_result.json' for i in range(0, 7)]
out_csv = 'output_cvs_file_orgs.csv'
all_data = []
headers = {'Accept': 'application/json'}


def request_ror_api(name):
    """request ror api"""
    base = 'https://api.ror.org/organizations?affiliation='
    url = f'{base}{quote(name)}'
    req_json = {}
    # print(url)
    req = requests.get(url, headers=headers, timeout=(10, 60))
    try:
        req_json = req.json()
    except requests.exceptions.JSONDecodeError:
        print('An exception occured during ror api request.')
        req_json = {}
    return req_json


offset = 6
for n, filei in enumerate(files[offset:]):
    j = n + offset
    print(filei)
    all_data = []
    with open(filei, 'r') as fileo:
        data = json.load(fileo)
    print(len(data['docs']))
    # continue
    with progressbar.ProgressBar(max_value=len(data['docs'])) as pbar:
        for i, item in enumerate(data['docs']):  # [:10]):
            pbar.update(i)
            id_ = item['id']
            json_content = item['json_source']
            type_ = item['type']
            name = item.get('name', None)
            if name is None:
                # print(item)
                ror_json = {}
            else:
                # print(i, name)
                ror_json = request_ror_api(name)
            num_res = ror_json.get('number_of_results', None)
            if num_res == 0 or num_res is None:
                ror_api_match = None
                ror_api_id = None
                ror_api_score = None
                ror_api_type = None
            else:
                ror_item = ror_json.get('items')[0]
                ror_api_match = ror_item['organization']['name']
                ror_api_id = ror_item['organization']['id']
                ror_api_score = ror_item['score']
                ror_api_type = ror_item['matching_type']

            all_data.append(
                {
                    'id': id_,
                    'name': name,
                    'type': type_,
                    'json_content': json_content,
                    'ror_api_match': ror_api_match,
                    'ror_api_score': ror_api_score,
                    'ror_id': ror_api_id,
                    'ror_api_match_type': ror_api_type,
                    'ror_api_output_json': ror_json,
                }
            )
    df = pd.DataFrame(all_data)
    df.to_csv(f'{j}_{out_csv}')
