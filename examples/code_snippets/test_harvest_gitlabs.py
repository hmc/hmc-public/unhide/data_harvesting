# -*- coding: utf-8 -*-
from .harvest_gitlabs import harvest_project


def test_harvest_project():
    url = 'https://jugit.fz-juelich.de/c.schiffer/typhon'
    giturl = 'git@jugit.fz-juelich.de:c.schiffer/typhon.git'
    harvest_project(giturl, url)
