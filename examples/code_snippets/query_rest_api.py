# -*- coding: utf-8 -*-
import json

import requests

# execute a certain API query on API.

# Specify the virtuose DB endpoint
query = 'search?document_type=institutions&start=0&rows=10'
outputfile = 'api_result.json'
endpoint = 'https://api.unhide.helmholtz-metadaten.de/'
per_page = 10000

headers = {'Accept': 'application/json'}
full_query = endpoint + query
res = requests.get(full_query, headers=headers, timeout=(10, 60))

res_json = res.json()
n_records = res_json['counts']['institutions']

for i in range(0, n_records // per_page + 1):
    print(i)
    full_query = f'{endpoint}search?document_type=institutions&start={(i)*per_page}&rows={per_page}'
    print(i, full_query)
    res = requests.get(full_query, headers=headers, timeout=(10, 60))
    result = res.json()
    outputfilei_page = f'{i}_page{outputfile}'
    with open(outputfilei_page, 'w') as fileo:
        json.dump(result, fileo)
    # fileo.write(j_res)
