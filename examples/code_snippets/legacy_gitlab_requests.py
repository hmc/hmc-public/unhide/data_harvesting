# -*- coding: utf-8 -*-
import json
import os


from data_harvesting.harvester.git import request_gitlab
from data_harvesting.harvester.git import walk_gitlab

# we do this: curl --head --request GET "https://jugit.fz-juelich.de/api/v4/projects" > jugit_projects.json
# https://jugit.fz-juelich.de/api/v4/projects?pagination=keyset&non_archived=true&page=2&sort=desc&order_by=id&visibility_level=20&per_page=100
# walking the pages is not trivial, there are links to next pages
"""
{'Date': 'Mon, 10 Oct 2022 13:57:41 GMT', 'Server': 'Apache/2.4.41 (Ubuntu)', 'Cache-Control': 'max-age=0, private, must-revalidate', 'Content-Type': 'application/json', 'Etag': 'W/"6464f11edaeef1b44cf221be7f278ac1"', 'Link': '<https://jugit.fz-juelich.de/api/v4/projects?id_before=7183&imported=false&membership=false&non_archived=true&order_by=id&owned=false&page=1&pagination=keyset&per_page=2&simple=false&sort=desc&starred=false&statistics=false&visibility_level=20&with_custom_attributes=false&with_issues_enabled=false&with_merge_requests_enabled=false>; rel="next"', 'Vary': 'Origin', 'X-Content-Type-Options': 'nosniff', 'X-Frame-Options': 'SAMEORIGIN', 'X-Request-Id': '01GF12ZRAN75XCXWGY8N903EJA', 'X-Runtime': '0.075538', 'Content-Length': '1918', 'Strict-Transport-Security': 'max-age=15768000;includeSubdomains', 'Keep-Alive': 'timeout=5, max=100', 'Connection': 'Keep-Alive'}

"""
gitlabs = {
    'FZJ': [
        {'name': 'jugit', 'url': 'https://jugit.fz-juelich.de'},
        {'name': 'gitlab_jsc', 'url': 'https://gitlab.jsc.fz-juelich.de/'},
        {'name': 'iffgit', 'url': 'https://iffgit.fz-juelich.de/'},
        # {"name" : "gitlab_public", "url" : "https://gitlab-public.fz-juelich.de/"}, # same as jugit
        {'name': 'cosy_gitlab', 'url': 'https://gitlab.cce.kfa-juelich.de'},
    ],
    'HZDR': [{'name': 'gitlab_hzdr', 'url': 'https://codebase.helmholtz.cloud/'}],
    'HZB': [{'name': 'gitlab_helmholtz-berlin', 'url': 'https://gitlab.helmholtz-berlin.de/'}],
    'GFZ': [{'name': 'git_gfz-potsdam', 'url': 'https://git.gfz-potsdam.de/'}],
    'DESY': [
        {'name': 'gitlab_desy', 'url': 'https://gitlab.desy.de'},
        {'name': 'eosc_pan_git_desy', 'url': 'https://eosc-pan-git.desy.de'},
        {'name': 'gitlab_zeuthen', 'url': 'https://gitlab.zeuthen.desy.de'},
        {'name': 'gitlab_cta', 'url': 'https://gitlab.cta-observatory.org', 'comment': 'Partner only'},
    ],
    'Helmholtz-muenchen': [{'name': 'ascgitlab', 'url': 'https://ascgitlab.helmholtz-muenchen.de/'}],
    'GEOMAR': [{'name': 'git_geomar', 'url': 'https://git.geomar.de/'}],
    # {"name" : "dm_git_geomar", "url" : "https://dm-git.geomar.de/"}],  # same as git.geomar
    'KIT': [
        {'name': 'git_scc_kit', 'url': 'https://git.scc.kit.edu'},
        {'name': 'itiv_kit', 'url': 'https://gitlab.itiv.kit.edu'},
        {'name': 'particle_kit', 'url': 'https://git.particle.kit.edu'},
    ],
    'UFZ': [{'name': 'git_ufz', 'url': 'https://git.ufz.de'}],
    'DLR': [
        {'name': 'gitlab_dlr', 'url': 'https://gitlab.dlr.de'},
        {'name': 'dlras', 'url': 'https://gitlab.as.dlr.de'},
    ],  # https://git.gnc.dlr.de/users/sign_in
    'AWI': [{'name': 'git_awi', 'url': 'https://gitlab.awi.de'}],
    'GSI': [
        {'name': 'git_gsi', 'url': 'https://git.gsi.de'},
        {'name': 'cbm_gsi', 'url': 'https://git.cbm.gsi.de'},
        {'name': 'panda_gsi', 'url': 'https://git.panda.gsi.de'},
    ],
    'CISPA': [{'name': 'cispa_saarland', 'url': 'https://projects.cispa.saarland'}],
}

name = 'ALL'


def request_gitlab(name):
    """Get all projects from API for a given HGF center"""
    for entry in gitlabs[name]:
        base_link = entry['url']
        git_name = entry['name']
        base_savepath = f'./gitlab_project_jsons/{git_name}'
        link = (
            f'{base_link}/api/v4/projects?pagination=keyset&non_archived=true&page=1&sort=desc&order_by=id&visibility_level=20&per_page=100'
        )

        os.makedirs(base_savepath, exist_ok=True)

        headers = []
        projects_json_file = f'{base_savepath}/{git_name}_projects_headers.json'

        headers, results_files = walk_gitlab(start_link=link, destination_folder=base_savepath, gitlab_name=git_name)

        with open(projects_json_file, 'w') as fileo:
            for header in headers:
                fileo.write(str(header))

        projects_json_file1 = results_files[0]
        with open(projects_json_file1, 'r') as fileo:
            projects_json = json.load(fileo)

        print(f'NProjects: {len(projects_json)}')

        for project in projects_json:
            print(project['name'])


if __name__ == '__main__':  # pragma: no cover
    if name == 'ALL':
        for center, val in gitlabs.items():
            request_gitlab(center)
    else:
        request_gitlab(name)

# To create a merge request gitlab API
# POST /projects/:id/merge_requests
