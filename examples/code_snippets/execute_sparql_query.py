# -*- coding: utf-8 -*-
import json

from SPARQLWrapper import JSONLD
from SPARQLWrapper import SPARQLWrapper

# Does not work currently, it recives text/haml instead of an other format, and that cannot be converted...
# so there could be a bug in the wrapper, that the request to jena is wrong...

# Specify the DB endpoint
database_name = 'playground2'
outputfile = 'sparql_result.json'
endpoint = f'http://localhost:3030/{database_name}/sparql'

sparql = SPARQLWrapper(endpoint)
sparql.setReturnFormat(JSONLD)

query_str = """
prefix x: <https://schema.org>
CONSTRUCT {?subject ?predicate ?object}
WHERE {
  :A (x:|!x:)* ?subject ?predicate ?object .
}
LIMIT 1000
"""
query_str = """
CONSTRUCT {?subject ?predicate ?object}
"""

sparql.setQuery(query_str)

# Convert results to JSON format
result = sparql.queryAndConvert()  # query().convert()
print(type(result))
# j_res = result.serialize()#format='json-ld')

# print(j_res)
with open(outputfile, 'w') as fileo:
    json.dump(result, fileo)
    # fileo.write(j_res)
