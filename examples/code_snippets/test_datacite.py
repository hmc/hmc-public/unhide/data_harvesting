# -*- coding: utf-8 -*-
import json

import requests

"""
from datacite import DataCiteRESTClient, schema42


doi = 'https://doi.org/10.1088/1361-6471/ab28f5'
doi = '10.1088/1361-6471/ab28f5'
# Initialize the REST client.

d = DataCiteRESTClient(
    username="MYDC.MYACCOUNT",
    password="mypassword",
    prefix="10.1234",
    test_mode=True
)


b= d.doi_get(doi)
print(b)
a = get_metadata(doi)
print(a)

"""

url_doi = 'https://api.datacite.org/application/vnd.schemaorg.ld+json/10.1088/1361-6471/ab28f5'

req = requests.get(url_doi)
jsonres = req.json()
print(jsonres)

# filter dois
filter_doi_url = 'https://api.datacite.org/dois?provider-id=ruwk&affiliation=true'
# ruwk is awi
url = 'https://api.datacite.org/providers'

req = requests.get(url)
jsonres = req.json()
# print(jsonres)
with open('data_cite_prov.json', 'w') as fileo:
    json.dump(jsonres, fileo, indent=4, separators=(', ', ': '), sort_keys=True)
