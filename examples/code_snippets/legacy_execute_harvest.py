# -*- coding: utf-8 -*-
import json
import os
import threading
# from data_harvesting.gitlab.harvest_gitlabs import harvest_fullgitlab

hgf_name = 'ALL'
use_threading = False  # currently there is some bug with paths while multi threading,
# the project jsons are not found

# read in gitlabs:
gitlabs = {}
with open('gitlabs.json', 'r') as fileo:
    gitlabs = json.load(fileo)

# print(gitlabs)


def harvest_hgf_gitlabs(name, base_savepath=None):
    """Clone all projects for a given HGF center and harvest codemeta.jsons"""

    for entry in gitlabs[name]:
        base_link = entry['url']
        git_name = entry['name']
        if base_savepath is None:
            savepath = os.path.abspath(f'./gitlab_project_jsons/{git_name}')
        else:
            savepath = base_savepath
        project_files = []
        for files in os.listdir(savepath):
            if files.endswith('.json'):
                if 'header' not in files:
                    project_files.append(os.path.abspath(os.path.join(savepath, files)))

        print(project_files)
        for projects_json_file in project_files:
            harvest_fullgitlab(projects_json_file, gitlab_name=git_name)


if __name__ == '__main__':  # pragma: no cover
    if hgf_name == 'ALL':
        threads = []
        for center, val in gitlabs.items():
            if use_threading:
                # for now one thread per center
                thread = threading.Thread(target=harvest_hgf_gitlabs, kwargs={'name': center})
                threads.append(thread)
                thread.start()
                # time.sleep(10) # otherwise there is some io problem...
            else:
                harvest_hgf_gitlabs(center)

        for threads in threads:
            thread.join()
    else:
        harvest_hgf_gitlabs(hgf_name)
