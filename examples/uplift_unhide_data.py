# -*- coding: utf-8 -*-
from data_harvesting.util.data_model_util import apply_aggregator

folderpath = '/home/j.broeder/work/data/hgf_kg/datapubs/juelichdata'
config_path = '/home/j.broeder/work/git/data-harvesting/data_harvesting/configs/config.yaml'
apply_aggregator(folderpath, config=config_path, overwrite=True)
