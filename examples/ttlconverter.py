# -*- coding: utf-8 -*-
from pathlib import Path

import progressbar

from data_harvesting.util.json_ld_util import convert

ld_folder = './pangaea'
dest_folder = './pangaea_ttl'

dir_path = Path(ld_folder).resolve()
all_files = list(dir_path.glob('**/*.json'))
# print(all_files)

nfiles = len(all_files)
print(f'Converting {nfiles} jsonld files from {dir_path} to ttl.')
with progressbar.ProgressBar(max_value=nfiles) as bar:
    for i, ldfile in enumerate(all_files):
        bar.update(i)
        outname = ldfile
        # outname.replace('.json', '.ttl')
        base_pa = outname.parent
        name = str(outname.name).replace('.json', '.ttl')
        if dest_folder is not None:
            outname = Path(dest_folder) / name
        else:
            outname = base_pa / name
        if outname.exists():
            # print('File_already exists')
            continue
        try:
            convert(ldfile, outname)
        except AttributeError as er:
            print(f'Failed convertion of {ldfile}:' f'AttributeError, could not convert json-ld; {str(er)}')
            continue
