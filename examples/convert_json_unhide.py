# -*- coding: utf-8 -*-
from data_harvesting.util.data_model_util import convert_json_unhide

metadata = {}
metadata['harvester_class'] = 'SitemapHarvester'  #'DataciteHarvester'

folderpath = '/home/j.broeder/work/data/hgf_kg/datapubs/juelichdata'
convert_json_unhide(folderpath, metadata=metadata, overwrite=True, infrmt='json')
