# -*- coding: utf-8 -*-
import pprint
import time

import atoma
import extruct
import requests
from w3lib.html import get_base_url

url = 'https://events.hifis.net/event/929/'
# sitemap gives this  https://data.fz-juelich.de/dataset.xhtml?persistentId=doi:10.26165/JUELICH-DATA/VGEHRD

token = 'indp_3SicijJvemsUSbm18kl9dEDk2OmAPXK1h2r4OtJQAF'
# https://events.hifis.net/export/event/search/ichep.json?ak=00000000-0000-0000-0000-000000000000&pretty=yes:

pp = pprint.PrettyPrinter(indent=2)
r = requests.get(url)
base_url = get_base_url(r.text, r.url)
syntaxes = ['dublincore', 'json-ld', 'microdata', 'opengraph', 'rdfa']
data = extruct.extract(r.text, syntaxes=syntaxes, base_url=base_url)

pp.pprint(data)

headers = {
    'Authorization': f'Bearer {token}',
    'Content-Type': 'application/json',
    'Accept': 'application/xml',
    'Connection': 'keep-alive',
}
base_url = 'https://events.hifis.net'

# https://indico.scc.kit.edu
# /export/categ/2.json?from=2010-01-01&to=future&pretty=yes
# /export/categ/2.json?from=today&to=future&pretty=yes
# harvest later from lastrun to +2 years
categories = [150, 162, 12, 161]
event_ids = []
titles = []
urls = []
keywords = []

for cat in categories:
    feed_cat = f'/category/{cat}/events.atom'
    url_r = base_url + request_cat
    req = requests.get(url_r, headers=headers)
    feed = atoma.parse_atom_bytes(req.content)
    for event in feed.entry:
        id_ev = event.id  # get('id', None) #url, keywords, title
        updated = event.updated
        if id_ev is not None:
            event_ids.append(id_ev)
        # titles.append(event.get('title', ''))
        url_ev = id_ev
        time.sleep(0.5)
        req2 = requests.get(url_ev)
        base_url_ex = get_base_url(req2.text, req2.url)
        syntaxes = ['json-ld']
        json_ld = extruct.extract(r.text, syntaxes=syntaxes, base_url=base_url_ex).get('json-ld')[-1]
        keywords = data.get('keywords', None)
        if keywords:
            json_ld['keywords'] = keywords
        json_ld['@id'] = url_ev
        print(json_ld)
"""
for cat in categories:
    print(cat)
    request_cat = f'/export/categ/{cat}' +'.json?from=2004-01-01&to=2026-01-01&pretty=yes'
    url_r = base_url + request_cat
    print(url_r)
    req = requests.get(url_r, headers=headers)

    data = req.json()
    events = data.get('results', [])
    if len(events) == 0:
        pp.pprint(data)
    for event in events:
        id_ev = event.get('id', None) #url, keywords, title
        if id_ev is not None:
            event_ids.append(id_ev)
        #titles.append(event.get('title', ''))
        url_ev = event.get('url')
        time.sleep(0.5)
        req2 = requests.get(url_ev)
        base_url_ex = get_base_url(req2.text, req2.url)
        syntaxes = ['json-ld']
        json_ld = extruct.extract(r.text, syntaxes=syntaxes, base_url=base_url_ex).get('json-ld')[-1]
        keywords = data.get('keywords', None)
        if keywords:
            json_ld['keywords'] =  keywords
        json_ld['@id'] = url_ev
        print(json_ld)


print(event_ids)
print(len(events), len(event_ids))
#pp.pprint(list(data.keys()))
#print(titles)
#pp.pprint(data.get('results'))
#pp.pprint(r.json())


#def harvest_by_range(url, start, stop,
"""
