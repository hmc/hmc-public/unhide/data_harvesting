In this folder you can put external schemas against which the data (json-ld) should be validated


for example the schema.org schema:
https://schema.org/version/latest/schemaorg-current-https.jsonld

shacl shapes of schema.org:
https://datashapes.org/schema.jsonld

or the codemeta.json schema:
https://doi.org/10.5063/schema/codemeta-2.0
 
