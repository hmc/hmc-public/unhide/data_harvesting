# This queries were generated automatically.

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("Forschungszentrum J\u00fclich, Germany"
"Forschungszentrum J\u00fclich GmbH"
"Forschungszentrum J\u00fclich"
"Forschungszentrum Jülich GmbH"
"Forschungszentrum Jülich FZJ"
"(Forschungszentrum Jülich, Germany)"
"Forschungszentrum Juelich GmbH"
"(Forschungszentrum Jülich)"
"Forschungszentrum Jülich"
"Forschungszentrum Juelich"
"(Forschungszentrum Jülich GmbH)"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("GEOMAR Helmholtz Centre for Ocean Research Kiel, Germany"
"GEOMAR Helmholtz Centre for Ocean Research Kiel, Wischhofstr. 1-3, 24148 Kiel, Germany"
"GEOMAR Helmholtz Centre for Ocean Research Kiel, Germany(1)"
"GEOMAR - Helmholtz Centre for Ocean Research Kiel"
"GEOMAR Helmholtz Centre for Ocean Research Kiel , Germany"
"GEOMAR Helmholtz Centre for OceanResearch Kiel"
"GEOMAR Helmholtz Centre for Ocean Research Kiel, 24148 Kiel, Germany"
"GEOMAR Helmholtz Centre for Ocean Research Kiel"
"GEOMAR, Kiel, Germany"
"Geomar, Kiel, Germany"
"Geomar"
"GEOMAR Helmholtz-Zentrum für Ozeanforschung Kiel"
"GEOMAR Helmholtz-Zentrum für Ozeanforschung Kiel, Germany(1)"
"GEOMAR Helmholtz Centre for Ocean Research, Kiel, Germany"
"Helmholtz-Zentrum für Ozeanforschung GEOMAR, Kiel"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("Deutsches Elektronen-Synchrotron (DESY)"
"Deutsches Elektronen-Synchrotron"
"DESY"
" DESY"
"Deutsches Elektron-Synchrotron, DESY, Hamburg"
"desy"
"Deutsches Elektronen-Synchrotron"
"Deutsches Elektronen-Synchrotron, DESY, Hamburg"
"Deutsches Elektronen-Synchrotron (DESY)"
"Deutsches Elektronen-Synchrotron, DESY, Hamburg]"
"Deutsches Elektronen-Synchrotron DESY"
"Deutsches Elektronen-Synchrotron DESY, 22607 Hamburg, Germany"
"Deutsches Elektronen Synchrotron DESY"
"Deutsches Elektronen-Synchrotron, DESY"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("HZB"
"Helmholtz-Zentrum Hereon, Germany"
"Helmholtz-Zentrum Hereon, Geesthacht, Germany"
"Helmholtz-Zentrum Hereon"
"Helmholtz-Zentrum hereon, Geesthacht, Germany"
"Helmholtz Centre hereon GmbH, Geesthacht, Germany"
"Helmholtz-Zentrum Hereon: Geesthacht, Geesthacht, Germany"
"hereon"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences, Telegrafenberg, 14473 Potsdam, Germany"
"GFZ-Potsdam, Germany"
"GeoForschungsZentrum (GFZ), Germany"
"GFZ Deutsches GeoForschungsZentrum Potsdam, Germany"
"Deutsches GeoForschungsZentrum GFZ, , Telegrafenberg, 14473 Potsdam, Germany(1)"
"GFZ German Research Centre for Geosciences, Telegrafenberg, 14473 Potsdam, Germany"
"GFZ German Research Centre for Geosciences, Germany(1)"
"GFZ German Research Centre for Geosiences, Potsdam, Germany"
"GFZ German Research Centre for Geosciences, Potsdam, Germany"
"GFZ German Research Centre for Geosciences Potsdam, Germany"
"GFZ-Potsdam"
"Helmholtz Centre Potsdam / Deutsches Geoforschungszentrum GFZ, Germany"
"Deutsches GeoForschungsZentrum, Potsdam, Germany"
"GFZ German Research Centre for Geosciences, Geophysical Imaging, Potsdam, Germany"
"GFZ German Research Centre for Geosciecnces, Potsdam, Germany"
"Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences"
"Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum GFZ"
"GFZ Gerrman Reserach Centre for Geosciences, Potsdam, Germany"
"Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences, Potsdam, Germany"
"Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum (GFZ) "
"GFZ German ResGFZ German Research Centre for Geosciences, Potsdam, Germany"
"GFZ German Research Cenre for Geosciences, Potsdam, Germany"
"GFZ German Research Cenntre for Geosciences, Potsdam, Germany"
"Helmholtz Centre Potsdam, GFZ German Research Centre for Geosciences"
"Helmholtz Centre Potsdam, GFZ German Research Centre for Geosciences, Potsdam"
"Helmholtz Centre Potsdam, GFZ German Research Centre for Geosciences, Telegrafenberg, 14473 Potsdam, Germany"
"1. GFZ German Research Centre For Geosciences, Potsdam, Germany"
"Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences"
"German Research Centre for Geosciences, Potsdam, 14473, Germany"
"GFZ German Research Centre for Geosciences, and GFZ German Research Centre for Geosciences, Potsdam, Germany"
"GFZ German Research Centre for Geosciences (Germany)"
"Formerly at GFZ German Research Centre for Geosciences, Potsdam, Germany"
"Helmholtz-Zentrum Potsdam - Deutsches GeoForschungsZentrum GFZ"
"Deutsches GeoForschungsZentrum GFZ "
"GFZ Potsdam"
"Deutsches GeoForschungsZentrum GFZ, , Telegrafenberg, 14473 Potsdam, Germany"
"Helmholtz-Zentrum Potsdam - Deutsches GeoForschungsZentrum GFZ "
"GFZ German Reseach Centre for Geosciences"
"GFZ, Germany"
"GFZ German Research Centre of Geosciences, Potsdam, Germany"
"GFZ German Research Centre for Geosciences, Telegrafenberg, 14473 Potsdam"
"Deutsches GeoForschungsZentrum GFZ, Potsdam, Germany"
"GFZ German Research Centre For Geosciences, Potsdam, Germany"
"Deutsches Geoforschungszentrum – GFZ, Germany"
"GFZ German Research Centre for Geosciences, Potsdam, Germany Potsdam, Germany"
"GFZ Deutsches Geoforschungszentrum, Potsdam(3)"
"German Research Centre for Geosciences GFZ, Potsdam, Germany"
"GFZ Potsdam, Germany"
"GFZ German Research Centre for Geoscience Helmholtz Centre Potsdam, Potsdam, Germany"
"German Research Centre for Geosciences, Potsdam, Germany"
"GFZ German Research Centre for Geosciences, Potsdam, Gemany"
"GFZ German Research Centre for Geosciences; Section 5.3 Geomicrobiology; University of Potsdam, Institute of Earth and Environmental Science"
"GFZ German Research Centre for Geosciences, Potsdam, Germany; University of Potsdam, Institute of Geosciences, Potsdam, Germany"
"Helmholtz Centre Potsdam, GFZ German Research Centre for Geosciences, Potsdam, Germany"
"GFZ German Research Centre for Geosciencesm Potsdam, Germany"
"GFZ"
"GFZ German Research Centre for Geosciences Potsdam, Potsdam, Germany"
"GFZ German Research Centre for Geosciences. Potsdam, Germany"
"Deutsches GeoForschungsZentrum GFZ"
"German Research Center for Geoscience (GFZ), Germany"
"Helmholtz Centre Potsdam, GFZ German Research Centre for Geosciences, Germany"
"Helmholtzzentrum Potsdam, GFZ German Research Center for Geosciences, Telegrafenberg, 14473 Potsdam, Germany"
"German Research Centre for Geosciences (GFZ), Potsdam, Germany"
"German Research Centre for Geosciences, Germany"
"(https://www.gfz-potsdam.de/staff/daniel-eggert/)"
"Deutsches GeoForschungsZentrum"
"German Research Center for Geosciences, Telegrafenberg, 14473 Potsdam, Germany"
"GFZ German Research Centre for Geosciences, Potsdam, German"
"GFZ German Research Centre for Geosciences "
"German Research Centre for Geosciences GFZ Potsdam, Germany"
"GFZ, Potsdam, Germany"
"Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences, Telegrafenberg, 14473 Potsdam, Germany"
"GFZ German Reseach Centre for Geosciences, Potsdam, Germany"
"GFZ German Research Center for Geosciences, Potsdam, Germany"
"GFZ German Research Centre for Geosciences, Germany"
"GFZ German Research Centre for GeosciencesGFZ German Research Centre for Geosciences, Potsdam, Germany"
"University of Potsdam, GFZ German Research Centre for Geosciences"
"Helmholtz-Zentrum Potsdam - Deutsches GeoForschungsZentrum (GFZ)"
"GFZ German Research Centre for Geosciences,Potsdam, Germany"
"GFZ German Research Center for Geosciences"
"Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences, Germany"
"German Research Centre for Geosciences GFZ, Potsdam, Germany "
"GFZ German Research Science for Geosciences, Potsdam, Germany"
"Helmholtz Centre Potsdam, German Research Centre for Geosciences GFZ, Potsdam, Germany"
"GFZ Potsdam - Research Institute for Geosciences"
"Helmholtz-Zentrum Potsdam Deutsches GeoForschungsZentrum GFZ, Germany"
"GFZ German Research Centre for Geosciences, Potsdam, Germany (retired)"
"German Research Centre for Geoscience, Potsdam, Germany"
"GFZ German Research Centre for Geosciences, Potsdam. Germany"
"GFZ German Research Centre for Geosciences, Potsdam, Gerrmany"
"GFZ German Research Centre for Geosciences, Potsdam"
"GFZ German Research Centre for Geosciences, Scientific Drilling ICDP"
"GFZ German Research Centre for Geosciences, Potsdam, German; Free University Berlin, Berlin, Germany"
"Deutsches Geoforschungszentrum GFZ, Potsdam"
"Helmholtz-Zentrum Potsdam, Deutsches Geo-Forschungs-Zentrum, Germany"
"GFZ German Research Centre for Geosciences"
"German Research Centre for Geosciences GFZ"
"GFZ German Research Centre for Geosciences, Potsdam University"
"GFZ German Research Centre for Geosciences, Potsdam, Germany(1)"
"Helmholtz Center Potsdam - German Research Center for Geosciences, Potsdam, Germany"
"German Research Center for Geosciences (GFZ)"
"Helmholtz Centre Potsdam, GFZ German Research Science for Geosciences"
"GFZ German Research Centre for Geosciences, Potsdam, Germany; University of Potsdam, Potsdam, Germany"
"GFZ Potsdam Germany"
"GFZ German Research Centre for Geosciences, Potsdam (Germany)"
"Helmholtz Open GFZ German Research Centre for Geosciences, Potsdam, Germany"
"GFZ German Research Centre for Geoscicences, Potsdam, Germany"
"Helmholtz Centre Potsdam, German Research Center for Geosciences (GFZ), Potsdam, Germany"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("Deutsches Zentrum für Luft- und Raumfahrt e.V. (DLR)"
"German Aerospace Centre"
"German Aerospace Center"
"Deutsches Zentrum für Luft- und Raumfahrt DLR"
"Deutsches Zentrum für Luft- und Raumfahrt"
"DLR"
" Deutsches Zentrum für Luft- und Raumfahrt"
"German Aerospace Center (DLR)"
"Deutsches Zentrum für Luft- und Raumfahrt  e. V."
"Deutsches Zentrum für Luft- und Raumfahrt (DLR)"
"DLR e.V., Köln"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("Helmholtz-Zentrum Dresden-Rossendorf, Germany"
"Helmholz-Zentrum Dresden-Rossendorf"
"HZDR"
"Helmholtz-Zentrum Dresden-Rossendorf (HZDR), 01328 Dresden, Germany"
"Helmholtz-Zentrum Dresden-Rossendorf, Bautzner Landstra\u00dfe 400, 01328 Dresden, Germany"
"Helmholtz-Zentrum Dresden - Rossendorf (HZDR)"
"Helmholtz-Zentrum Dresden-Rossendorf, Bautzner Landstrasse 400, 01328 Dresden, Germany"
"Helmholz Zentrum Dresden - Rossendorf (HZDR)"
"Helmholtz Zentrum Dresden Rossendorf"
"Helmholtz-Zentrum Dresden-Rossendorf, 01314 Dresden, Germany"
"Helmholtz-Zentrum Dresden-Rossendorf"
"Helmholtz-Zentrum Dresden-Rossendorf (HZDR)"
"Helmholtz-Zentrum Dresden Rossendorf"
"Helmholtz-Zentrum Dresden-Rossendorf e.V."
"Helmholtz-Zentrum Dresden-Rossendorf, Dresden, Germany"
"Helmholtz Zentrum Dresden Rossendorf"
" HZDR: Helmholtz-Zentrum Dresden-Rossendorf"
"  HZDR: Helmholtz-Zentrum Dresden - Rossendorf"
"HZDR: Helmholtz Zentrum Dresden Rossendorf, Dresden, Germany"
" Helmholtz Zentrum Dresden Rossendorf"
"Helmholtz-Zentrum Dresden-Rossendorf"
" Forschungszentrum Dresden Rossendorf"
"Helmholtz-Zentrum Dresden-Rossendorf, Bautzner Landstrasse 400, 01328 Dresden, Germany"
"Helmholtz-Zentrum Dresden - Rossendorf (HZDR)"
"Helmholtz-Zentrum Dresden-Rossendorf (HZDR)"
"Helmholtz-Zentrum Dresden-Rossendorf, 01314 Dresden, Germany"
"Helmholtz-Zentrum Dresden-Rossendorf, Bautzner Landstraße 400, 01328 Dresden, Germany"
" HZDR: Helmholtz-Zentrum Dresden - Rossendorf"
"Helmholz-Zentrum Dresden-Rossendorf"
"Helmholtz-Zentrum Dresden-Rossendorf e.V."
"Helmholtz-Zentrum Dresden-Rossendorf, Dresden, Germany"
"Helmholtz-Zentrum Dresden Rossendorf"
"Helmholtz-Zentrum Dresden-Rossendorf, Germany"
"Helmholtz-Zentrum Dresden-Rossendorf HZDR"
"HZDR"
"Helmholz Zentrum Dresden - Rossendorf (HZDR)"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("Helmholtz Centre for Environmental Research"
"Helmholtz-Zentrum für Umweltforschung UFZ"
"Helmholtz Centre for Environmental Research – UFZ"
"Helmholtz-Zentrum für Umweltforschung - UFZ"
"Helmholtz Centre for Environmental Research - UFZ, Leipzig, Germany"
"Helmholtz Centre for Environmental Research - UFZ, Germany"
"Helmholtz Centre for Environmental Research GmbH, Germany"
"Helmholtz-Centre for Environmental Research"
"UFZ, Germany"
"Helmholtz Center for Environmental Research GmbH - UFZ"
"Helmholtz-Zentrum für Umweltforschung"
"UFZ Helmholtz Centre for Environmental Research"
"Helmholtz Centre for Environmental Research - UFZ"
"Helmholtz Centre for Environmental Research GmbH – UFZ, Germany"
"Helmholtz-Centre for Environmental Research – UFZ Leipzig, Permoserstrasse 15, 04318 Leipzig, Germany;"
"Helmholtz-Zentrum für Umweltforschung GmbH - UFZ"
"Helmholtz-Zentrum für Umweltforschung (UFZ), Germany"
"UFZ"
"Helmhotz-Zentrum für Umweltforschung UFZ, Germany"
"Helmholtz Centre for Environmental Research GmbH – UFZ"
"Hemholtz-Zentrum für Umweltforschung GmbH - UFZ"
"UFZ, Leipzig, Germany"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("GSI"
"GSI Helmholtzzentrum für Schwerionenforschung GmbH"
"GSI Helmholtz Centre for Heavy Ion Research"
"Helmholtzzentrum für Schwerionenforschung, 64291 Darmstadt, Germany"
" Darmstadt, GSI "
"GSI Helmholtzzentrum für Schwerionenforschung, Planckstraße 1, 64291 Darmstadt, Germany"
"GSI Helmholtzzentrum fuer Schwerionenforschung, GSI, Darmstadt"
"GSI Helmholtzzentrum für Schwerionenforschung"
"GSI Helmholtzzentrum fuer Schwerionenforschung, Darmstadt"
"GSI Helmholtzzentrum, Darmstadt"
"GSI Helmholtzzentrum fuer Schwerionenforschung"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("German Cancer Research Center (DKFZ), Heidelberg, Germany"
"German Cancer Research Center"
"German Cancer Research Center (DKFZ), Heidelberg, Germany"
"Deutsches Krebsforschungszentrum"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("KIT"
"Karlsruhe Institute of Technology"
"KIT (BMBF)"
"Karlsruher Institut für Technologie KIT"
"Karlsruher Institut für Technologie (KIT), Germany"
" KIT: Karlsruher Institut für Technologie"
"Karlsruher Institut für Technologie (KIT), Karlsruhe, Germany"
"Karlsruhe Institute of Technology"
"KIT, Karlsruhe"
"Karlsruhe Institute of Technology, Deutschland"
"Karlsruhe Institute of Technology (KIT), Germany(1)"
"KIT: Karlsruher Institut für Technologie"
"Karlsruhe Institute of Technology (KIT GPI Germany)"
"Karlsruhe Institute of Technology, Adenauerring 20b, 76131 Karlsruhe, Germany(3)"
"Karlsruher Institut für Technologie"
"Karlsruhe Institute of Technology, SCC"
"Karlsruhe Institut of Technology (KIT), Karlsruhe, Germany"
"Karlsruhe Institute of Technology, Germany"
" KIT"
"Karlsruher Institut für Technologie, Germany"
"Karlsruhe Institute for Technology, Germany"
"Institut für Physikalische Chemie, Karlsruhe Institute of Technology, Germany"
", Karlsruhe Institute of Technology, Deutschland"
"Karlsruhe Institute of Technology "
"Karlsruhe Institute of Technology, Institute of Applied Geosciences, Germany(1)"
"Karlsruhe Institute of Technology, Karlsruhe, Germany"
"Karlsruhe Institute of Technology KIT, Karlsruhe, Germany"
"Karlsruhe Institute of Technology (KIT), Karlsruhe, Germany"
"Karlsruhe Institute of Technology KIT"
"KIT, Germany"
"Karlsruher Institut für Technologie (KIT)"
"Karlsruhe institut für technologie, Germany"
"INT, Karlsruhe Institute of Technology, Germany"
"Karlsruhe Institute of Technology, Adenauerring 20b, 76131 Karlsruhe, Germany"
" Karlsruhe Institute of Technology"
"Karlsruhe Institute of Technology (KIT), Germany"
"Karlsruher Institute of Technology, Germany"
"KIT"
"KIT Karlsruhe Institute of Technology, Germany"
"Karlsruhe Institute of Technology (KIT)"
"Karlsruhe Institut of Technology"
"Karlsruhe Institut for Technology, Germany"
"KIT Karlsruhe Institute of Technology, Karlsruhe, Germany"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("German Center for Neurodegenerative Diseases"
"Deutsches Zentrum für Neurodegenerative Erkrankungen, Bonn"
"Deutsches Zentrum für Neurodegenerative Erkrankungen"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}

INSERT { ?res schema:affiliation ?org}
WHERE {
?res schema:affiliation ?affname.  . FILTER (STR(?affname) IN ("Helmholtz Zentrum München"))
?org a schema:Organization. FILTER (ISIRI(?org)).
?org schema:name ?orgname . FILTER (STR(?orgname) =STR(?affname))
}
