# This queries were generated automatically.

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/02nv7yv05> a schema:Organization ;
     schema:name "Forschungszentrum Jülich".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/02h2x0161> a schema:Organization ;
     schema:name "GEOMAR Helmholtz Centre for Ocean Research Kiel".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/01js2sh04> a schema:Organization ;
     schema:name "Deutsches Elektronen-Synchrotron DESY".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/03qjp1d79> a schema:Organization ;
     schema:name "Helmholtz-Zentrum Hereon".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/032e6b942> a schema:Organization ;
     schema:name "Alfred Wegener Institute for Polar and Marine Research".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/04p5ggc03> a schema:Organization ;
     schema:name "Max Delbrück Center for Molecular Medicine".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/03d0p2685> a schema:Organization ;
     schema:name "Helmholtz Centre for Infection Research".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/02aj13c28> a schema:Organization ;
     schema:name "Helmholtz-Zentrum Berlin für Materialien und Energie".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/04z8jg394> a schema:Organization ;
     schema:name "Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/04bwf3e34> a schema:Organization ;
     schema:name "German Aerospace Center".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/01zy2cs03> a schema:Organization ;
     schema:name "Helmholtz-Zentrum Dresden-Rossendorf".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/000h6jb29> a schema:Organization ;
     schema:name "Helmholtz Centre for Environmental Research".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/043j0f473> a schema:Organization ;
     schema:name "German Center for Neurodegenerative Diseases".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/02njgxr09> a schema:Organization ;
     schema:name "Helmholtz Center for Information Security".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/02k8cbn47> a schema:Organization ;
     schema:name "GSI Helmholtz Centre for Heavy Ion Research".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/04cdgtt98> a schema:Organization ;
     schema:name "German Cancer Research Center".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/04t3en479> a schema:Organization ;
     schema:name "Karlsruhe Institute of Technology".
}

PREFIX schema: <http://schema.org/>
INSERT {
  <https://ror.org/00cfam450> a schema:Organization ;
     schema:name "Helmholtz Zentrum München".
}
